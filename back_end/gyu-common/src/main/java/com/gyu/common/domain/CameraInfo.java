package com.gyu.common.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2023-12-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("camera_info")
public class CameraInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @TableField("camera_name")
    private String cameraName;

    @TableField("camera_code")
    private String cameraCode;

    @TableField("camera_location")
    private String cameraLocation;

    @TableField("camera_live_url")
    private String cameraLiveUrl;

    @TableField("longitude")
    private String longitude;

    @TableField("latitude")
    private String latitude;

    @TableField("height")
    private String height;

    @TableField("is_ok")
    private String isOk;

    @TableField("camera_history_video")
    private String cameraHistoryVideo;

    @TableField("base_height")
    private String baseHeight;


    @TableField(exist = false)
    private List<String> ids;
}
