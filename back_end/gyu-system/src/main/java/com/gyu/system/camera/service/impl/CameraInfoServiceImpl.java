package com.gyu.system.camera.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gyu.common.domain.CameraInfo;
import com.gyu.common.enums.BizCodeEnum;
import com.gyu.common.exception.BizException;
import com.gyu.common.utils.SnowFlakeIdWorker;
import com.gyu.system.camera.mapper.CameraInfoMapper;
import com.gyu.system.camera.service.CameraInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.netty.util.internal.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2023-12-25
 */
@Service
public class CameraInfoServiceImpl extends ServiceImpl<CameraInfoMapper, CameraInfo> implements CameraInfoService {

    @Autowired
    private SnowFlakeIdWorker snowFlakeIdWorker;

    @Autowired
    private CameraInfoMapper cameraInfoMapper;

    @Override
    public IPage findCameraByPage(String cameraName, String cameraCode, String cameraLocation, Integer pageNum, Integer pageSize) {
        IPage<CameraInfo> page = new Page<>(pageNum, pageSize);
        QueryWrapper<CameraInfo> wrapper = new QueryWrapper<>();

        if (StringUtils.isNotEmpty(cameraName)){
            wrapper.or().like("camera_name", cameraName);
        }
        if (StringUtils.isNotEmpty(cameraCode)) {
            wrapper.or().like("camera_code", cameraCode);
        }
        if (StringUtils.isNotEmpty(cameraLocation)) {
            wrapper.or().like("camera_location", cameraLocation);
        }
        IPage<CameraInfo> gyResidenceInfoIPage = this.page(page, wrapper);
        return gyResidenceInfoIPage;
    }

    /**
     * 创建相机
     * @param cameraInfo
     * @return
     */
    @Transactional
    @Override
    public boolean createCamera(CameraInfo cameraInfo) {
        String cameraName = cameraInfo.getCameraName();
        if (StringUtils.isNotEmpty(cameraName)){
            CameraInfo cameraByCameraName = this.findOneCameraByCameraName(cameraName);
            if (cameraByCameraName != null) {
                throw new BizException(500, "摄像头名称重复");
            }

            String cameraCode = cameraInfo.getCameraCode();
            CameraInfo cameraByCameraCode = this.findOneCameraByCameraCode(cameraCode);
            if (cameraByCameraCode != null){
                throw new BizException(500, "摄像头编号重复");
            }
        }

        if (cameraInfo != null){
            String id = snowFlakeIdWorker.getId();
            cameraInfo.setId(id);
            boolean save = this.save(cameraInfo);
            return  save;
        }

        return false;
    }

    /**
     * 根据ids批量删除摄像头
     * @param ids
     * @return
     */
    @Override
    public boolean deleteCameraById(List<String> ids) {
        int i = cameraInfoMapper.deleteBatchIds(ids);
        return i>0;
    }

    @Override
    public boolean updateCameraById(CameraInfo cameraInfo) {
        if (cameraInfo != null){
            boolean b = this.updateById(cameraInfo);
            return b;
        }
        return false;
    }

    @Override
    public List<CameraInfo> findAll() {
        return cameraInfoMapper.selectList(null);
    }

    /**
     * 根据摄像头名称查询一个
     * @param cameraName
     * @return
     */
    public CameraInfo findOneCameraByCameraName(String cameraName){
        QueryWrapper<CameraInfo> wrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(cameraName)) {
            wrapper.eq("camera_name", cameraName);
            CameraInfo cameraInfo = this.getOne(wrapper);
            return cameraInfo;
        }
        return null;
    }

    /**
     * 根据摄像头编码查询一个
     * @param cameraCode
     * @return
     */
    public CameraInfo findOneCameraByCameraCode(String cameraCode){
        QueryWrapper<CameraInfo> wrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(cameraCode)){
            wrapper.eq("camera_code", cameraCode);
            CameraInfo cameraInfo = this.getOne(wrapper);
            return cameraInfo;
        }
        return null;
    }
}
