import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import timezone from 'dayjs/plugin/timezone'
dayjs.extend(utc)
dayjs.extend(timezone)
dayjs.tz.guess() // America/Chicago
// import 'dayjs/locale/zh-cn' // 导入本地化语言
// dayjs.locale('zh-cn') // 使用本地化语言

// 获取最近一个月时间范围
export function getLastMouthRangeTime() {
    // var a = dayjs()
    var a = dayjs(new Date())
    var endTime = a.format('YYYY-MM-DDTHH:mm:ss.000+08:00')
    var startTime = a.subtract(1, 'day').format('YYYY-MM-DDTHH:mm:ss.000+08:00')
    return [startTime, endTime]
}
