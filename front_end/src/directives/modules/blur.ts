/*
 * @Author: Wang Wei 2419715149
 * @Date: 2023-09-03 11:22:36
 * @LastEditors: ww 92363803+lisrmww@users.noreply.github.com
 * @LastEditTime: 2023-09-03 11:35:22
 * @Description: ElButton 点击后自动失焦指令
 */
import type {Directive} from 'vue'
import {useEventListener} from '@vueuse/core'

export default {
    mounted(el) {
        useEventListener(el, 'focus', () => el.blur())
    },
} as Directive
