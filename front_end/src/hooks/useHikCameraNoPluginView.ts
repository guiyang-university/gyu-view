// 海康平台无插件预览版

//@ts-nocheck
import {getPreviewUrl} from '@/api/camera'
// import './h5player.min.js'
import {onMounted} from 'vue'

export function useHikCameraNoPluginView(elId: string) {
    let player: any = null

    // HTML容器id
    function createPlayer() {
        player = new window.JSPlugin({
            szId: 'elId',
            szBasePath: '/public', // 必填,与h5player.min.js的引用目录一致
            bSupporDoubleClickFull: false, //是否支持双击全屏，默认true
            // 分屏播放，默认最大分屏4*4
            // iMaxSplit: 4,
            // iCurrentSplit: 1,
            oStyle: {
                borderSelect: '#FFCC00',
            },
        })

        return player
    }

    onMounted(() => {
        createPlayer()
    })

    // 设置播放容器的宽高并监听窗口大小变化
    function resizeWin() {
        window.addEventListener('resize', () => {
            player.JS_Resize()
        })
    }

    return {createPlayer}
}

async function getUrl() {
    const res = await getPreviewUrl({
        cameraIndexCode: '', // 摄像头唯一编号
        protocol: 'ws',
    })

    if (res.code === 200) {
        return res.data.url
    }

    return ''
}
