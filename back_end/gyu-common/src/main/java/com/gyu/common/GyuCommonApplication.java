package com.gyu.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GyuCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(GyuCommonApplication.class, args);
    }

}
