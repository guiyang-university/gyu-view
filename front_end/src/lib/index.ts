/*
 * @Author: Wang Wei 2419715149
 * @Date: 2023-08-30 09:13:34
 * @LastEditors: ww 92363803+lisrmww@users.noreply.github.com
 * @LastEditTime: 2023-09-02 16:00:34
 * @Description: 自动注册组件
 */

import type {App} from 'vue'
import {defineAsyncComponent} from 'vue'

export default {
    install(app: App<Element>) {
        // 获取当前路径任意文件夹下的 index.vue 文件
        const components = import.meta.glob('./*/index.vue')
        // 遍历获取到的组件模块
        for (const [key, value] of Object.entries(components)) {
            // 拼接组件注册的 name
            const componentName = 'sm-' + key.replace('./', '').split('/')[0]
            // 通过 defineAsyncComponent 异步导入指定路径下的组件
            app.component(componentName, defineAsyncComponent(value as any))
        }
    },
}
