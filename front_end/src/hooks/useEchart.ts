import {onUnmounted, watch} from 'vue'
import * as echarts from 'echarts'
import type {EChartsCoreOption, ECharts} from 'echarts'

export default function useEcharts(
    domEl: HTMLElement,
    echartData: any,
    getOption: (echartData: any) => {},
): IuseEcharts {
    // let echartInstance = echarts.getInstanceByDom(domEl)

    // const echartInstance = echarts.init(domEl, undefined, { renderer: 'svg' })
    const echartInstance = echarts.init(domEl, undefined)

    echartInstance.setOption(getOption(echartData))

    onUnmounted(() => {
        echartInstance.dispose()
    })

    function resizeEchart() {
        echartInstance.resize()
    }

    function setOption(option: EChartsCoreOption) {
        echartInstance.setOption(option)
    }

    watch(
        () => echartData.value,
        () => {
            console.log(' echartInstance.setOption(getOption(echartData))')
            echartInstance.setOption(getOption(echartData))
        },
    )

    return {
        resizeEchart,
        setOption,
        echartInstance,
    }
}

export interface IuseEcharts {
    resizeEchart(): void
    setOption(option: EChartsCoreOption): void
    echartInstance: ECharts
}
