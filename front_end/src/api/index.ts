import Request from '@/utils/request'

const request = new Request({
    baseURL: import.meta.env.VITE_APP_DATA_URL,
    timeout: 0,
    headers: {
        'Content-Type': 'application/json; charset=utf-8',
    },
    interceptors: {
        responseInterceptors(res: any) {
            if (res.code !== 200) {
                ElMessage.error(res.message)
                return null
            }
            return res.data ? res.data : res
        },
    },
})

export default request
