package com.gyu.system.map.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gyu.common.domain.MapPic;
import com.gyu.common.response.ApiResult;
import com.gyu.system.config.MinIoUtils;
import com.gyu.system.map.service.MapPicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2023-12-27
 */
@RestController
@RequestMapping("/map/mapPic")
public class MapPicController {

    @Autowired
    private MinIoUtils minIoUtils;

    @Autowired
    private MapPicService mapPicService;

    /**
     * 上传地图图片
     * @param multipartFile
     * @param bucket
     * @return
     */
    @PostMapping("/upload")
    public ApiResult uploadMapPic(@RequestParam("file")MultipartFile multipartFile, @RequestParam(value = "bucket", defaultValue = "map") String bucket){
        try {
            if (multipartFile == null){
                return new ApiResult().error();
            }

            // 创建bucket
            boolean existBucket = minIoUtils.existBucket(bucket);
            if(!existBucket){
                boolean created = minIoUtils.makeBucket(bucket);
                if (created) {
                    String policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":[\"*\"]},\"Action\":[\"s3:ListBucket\",\"s3:ListBucketMultipartUploads\",\"s3:GetBucketLocation\"],\"Resource\":[\"arn:aws:s3:::" + bucket + "\"]},{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":[\"*\"]},\"Action\":[\"s3:AbortMultipartUpload\",\"s3:DeleteObject\",\"s3:GetObject\",\"s3:ListMultipartUploadParts\",\"s3:PutObject\"],\"Resource\":[\"arn:aws:s3:::" + bucket + "/*\"]}]}";
                    // 设置权限
                    minIoUtils.setBucketPolicy(bucket, policy);
                    // 存储文件
                    String fileUlr = minIoUtils.upload(multipartFile, bucket);
                    return  new ApiResult().success(fileUlr);
                }
            }
            // 存储文件
            String fileUrl = minIoUtils.upload(multipartFile, bucket);
            return new ApiResult().success(fileUrl);
        } catch (Exception e){
            e.printStackTrace();
        }

        return new ApiResult().error();
    }

    /**
     * 新增地图
     * @param mapPic
     * @return
     */
    @PostMapping("/save")
    public ApiResult saveMap(@RequestBody MapPic mapPic){
        boolean bool = mapPicService.saveMaPic(mapPic);
        if (bool) {
            return new ApiResult().success(bool);
        }

        return new ApiResult().error();
    }

    /**
     * 获取地图列表
     * @param mapName
     * @param mapType
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("/list")
    public ApiResult getMapList(@RequestParam(value = "mapName", required = false) String mapName,
                                @RequestParam(value = "mapType", required = false) String mapType,
                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                @RequestParam(value = "pageSize", defaultValue = "8") Integer pageSize){
        IPage pageInfo = mapPicService.findMapByPage(mapName, mapType, pageNum, pageSize);
        if (null != pageInfo) {
            return new ApiResult().success(pageInfo);
        }
        return new ApiResult().error();
    }

    @GetMapping("/layers")
    public ApiResult getAllLayers(){
        List<MapPic> all = mapPicService.findAll();
        if (null != all){
            return new ApiResult().success(all);
        }
        return new ApiResult().error();
    }
}
