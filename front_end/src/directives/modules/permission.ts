/*
 * @Author: Wang Wei 2419715149
 * @Date: 2023-09-08 11:22:36
 * @LastEditors: ww 92363803+lisrmww@users.noreply.github.com
 * @LastEditTime: 2023-09-08 11:35:22
 * @Description: 组件内权限指令
 * 判断方式：系统名.页面名.权限字段
 */
import type {Directive, DirectiveBinding} from 'vue'
import {useRoute} from 'vue-router'
import {usePermissionStore} from '@/stores/permission'

function hasPermission(el: any, auth = '') {
    const route = useRoute()
    const permissionStore = usePermissionStore()
    const pageName = route.fullPath
    const verifyPermission = `system.${pageName}.${auth}`
    const permission = permissionStore.permissions.find(
        (permission) => permission === verifyPermission,
    )
    if (!permission) {
        el.parentNode && el.parentNode.removeChild(el)
    }
}

export default {
    mounted(el, auth: DirectiveBinding<string>) {
        hasPermission(el, auth.value)
    },
} as Directive
