package com.gyu.system.map.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gyu.common.domain.MapPic;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2023-12-27
 */
public interface MapPicService extends IService<MapPic> {

    boolean saveMaPic(MapPic mapPic);

    IPage findMapByPage(String mapName, String mapType, Integer pageNum, Integer pageSize);

    List<MapPic> findAll();
}
