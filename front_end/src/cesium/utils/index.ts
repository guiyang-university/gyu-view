//@ts-nocheck
import * as Cesium from 'cesium'
import edgeData from '../data/GYU_Edge.json'

/**
 * 加载边界
 * @param viewer
 */
export const loadAreaEdge = (viewer: Cesium.Viewer) => {
    return new Promise<void>((resolve) => {
        Cesium.GeoJsonDataSource.load(edgeData, {
            clampToGround: true,
            stroke: Cesium.Color.YELLOW,
            strokeWidth: 5,
        }).then((dataSource) => {
            viewer.dataSources.add(dataSource)
            resolve()
        })
    })
}

/**
 *
 * @param viewer
 * @param url
 * @param tilesetHeight 瓦片高程
 */
export const load3DTileset = async (viewer: Cesium.Viewer, url: string, tilesetHeight: number) => {
    try {
        // const customShader = new Cesium.CustomShader({
        //     lightingModel: Cesium.LightingModel.UNLIT,
        //     // 修改建筑物样式
        //     fragmentShaderText: `
        //     void fragmentMain(FragmentInput fsInput, inout czm_modelMaterial material) {
        //         // 获取模型点位的position信息，包含高程信息
        //         vec3 position = fsInput.attributes.positionMC;
        //         //vec4 position = czm_inverseModelView * vec4(fsInput.attributes.positionEC, 1.0);  // 等于第一种写法

        //         // 根据高度设置渐变
        //         float strength = (position.z - 1100.0) / 25.0; // 1100.0 建筑的底面高度， 25建筑平均高度
        //         material.diffuse = vec3(strength, 0.1 * strength,  0.3* strength);

        //         // 动态光环
        //         // czm_frameNumber获取当前帧数 fract(x)返回x的小数部分
        //         float time = fract(czm_frameNumber / (60.0 * 10.0));
        //         time = abs(time - 0.5) * 2.0;
        //         float diff = abs(clamp((position.z - 1100.0)/55.0, 0.0, 1.0) - time);
        //         // step(edge, x)，如果x大于等于edge，返回1，否则返回0
        //         diff = step(0.01, diff);
        //         material.diffuse.rgb += vec3(0.5) * (1.0-diff);
        //     }
        // `,
        // })

        const tileset = await Cesium.Cesium3DTileset.fromUrl(url)

        // tileset.customShader = customShader

        const boundingSphere = tileset.boundingSphere
        const cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center)
        const surface = Cesium.Cartesian3.fromRadians(
            cartographic.longitude,
            cartographic.latitude,
            0,
        ) //倾斜数据中心点的笛卡尔坐标
        const offset = Cesium.Cartesian3.fromRadians(
            cartographic.longitude,
            cartographic.latitude,
            tilesetHeight,
        ) //带高程的新笛卡尔坐标
        //平移的距离
        const translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3()) //做差得到变换矩阵
        //基于平移的距离构建平移矩阵
        const matrix4 = Cesium.Matrix4.fromTranslation(translation)
        //重置模型矩阵
        tileset.modelMatrix = matrix4

        viewer.scene.primitives.add(tileset)
        viewer.zoomTo(
            tileset,
            new Cesium.HeadingPitchRange(6.277388569331658, -0.6850717273191806, 1300),
        )
    } catch (error) {
        console.error(`Error creating tileset: ${error}`)
    }
}
