import * as Cesium from "cesium";
import * as turf from "@turf/random";
import PolylineTrailMaterialProperty from "./material/PolylineTrailMaterialProperty";

export default class RectFlyLight {
  bbox: number[];
  constructor(viewer: Cesium.Viewer) {
    // 设置矩形区域
    this.bbox = [104.04, 30.61, 104.11, 30.67];
    // 创建随机点

    let points = turf.randomPoint(150, {
      bbox: this.bbox
    });
    // 通过生成的随机点生成线
    let features = points.features;
    features.forEach((item: any) => {
      // 获取点的经纬度
      let point = item.geometry.coordinates;
      // 根据点设置起始位置
      let start = Cesium.Cartesian3.fromDegrees(point[0], point[1], 0);
      // 根据点设置结束位置
      let end = Cesium.Cartesian3.fromDegrees(point[0], point[1], 200 + Math.random() * 3000);
      //   创建自定义线材质
      let polylineTrailMaterialProperty = new PolylineTrailMaterialProperty();
      //   创建线
      let flyLine = viewer.entities.add({
        polyline: {
          positions: [start, end],
          width: 2,
          material: polylineTrailMaterialProperty
        }
      });
    });
  }
}
