package com.gyu.common.exception;


import com.gyu.common.enums.BizCodeEnum;
import lombok.Data;

/**
 * 自定义异常类
 */
@Data
public class BizException extends RuntimeException {

    private Integer code;
    private BizCodeEnum bizCodeEnum;


    public BizException() {
        super(BizCodeEnum.ERROR.getMsg());
        this.code = BizCodeEnum.ERROR.getCode();
    }

    public BizException(Integer code, String msg) {
        super(msg);
        this.code = code;
    }

    public BizException(BizCodeEnum bizCodeEnum) {
        super(bizCodeEnum.getMsg());
        this.code = bizCodeEnum.getCode();
    }

    public BizException(BizCodeEnum bizCodeEnum, Object message) {
        super(bizCodeEnum.getMsg() + "(" + message + ")");
        this.code = bizCodeEnum.getCode();
    }

    public BizException(BizCodeEnum bizCodeEnum, Object message, Throwable cause) {
        super(bizCodeEnum.getMsg() + "(" + message + ")", cause);
        this.code = bizCodeEnum.getCode();
    }
}
