import {reactive} from 'vue'

// 充电桩饱和比例
export const chargingPileData = reactive([
    {
        value: 100,
        name: '广州占比',
        percentage: '0%',
        color: '#34D160',
    },
    {
        value: 50,
        name: '深圳占比',
        percentage: '0%',
        color: '#027FF2',
    },
    {
        value: 56,
        name: '东莞占比',
        percentage: '0%',
        color: '#8A00E1',
    },
    {
        value: 75,
        name: '佛山占比',
        percentage: '0%',
        color: '#F19610',
    },
    {
        value: 35,
        name: '中山占比',
        percentage: '0%',
        color: '#6054FF',
    },
    {
        value: 66,
        name: '珠海占比',
        percentage: '0%',
        color: '#00C6FF',
    },
])

// 流程监控的数据
export const processMonitoringData = reactive([
    {
        name: '正常',
        data: [70, 98, 88, 15, 66, 44, 19, 88, 36, 48, 12, 8],
    },
    {
        name: '异常',
        data: [42, 102, 63, 57, 75, 66, 23, 55, 73, 22, 24, 11],
    },
])

// 充电数据统计
export const chargingStatisticsData = reactive([
    {
        name: '一月',
        value: 18,
    },
    {
        name: '二月',
        value: 45,
    },
    {
        name: '三月',
        value: 9,
    },
    {
        name: '四月',
        value: 0,
    },
    {
        name: '五月',
        value: 0,
    },
    {
        name: '六月',
        value: 0,
    },
    {
        name: '七月',
        value: 0,
    },
])
// 异常监控
export const exceptionMonitoringData = reactive([
    {
        id: 1,
        name: '异常1',
        value: 5,
        dur: '10s',
        begin: '0s',
    },
    {
        id: 2,
        name: '异常2',
        value: 3,
        dur: '10s',
        begin: '-3s',
    },
    {
        id: 3,
        name: '异常3',
        value: 5,
        dur: '10s',
        begin: '-5s',
    },
])

// 充电桩数据分析
export const dataAnalysisData = reactive([
    {
        id: 1,
        title: '充电桩总数(个)',
        totalNum: 0,
        unit: '万',
        percentage: 0,
        isUp: true,
    },
    {
        id: 2,
        title: '年增长总数(个)',
        totalNum: 0,
        unit: '万',
        percentage: 0,
        isUp: true,
    },
    {
        id: 3,
        title: '月增长总数(个)',
        totalNum: 0,
        unit: '万',
        percentage: 0,
        isUp: false,
    },
])

// 充电桩Top4占比
export const chargingTop4Data = reactive([
    {
        id: 1,
        name: '深圳',
        percentage: '0%',
    },
    {
        id: 2,
        name: '广州',
        percentage: '0%',
    },
    {
        id: 3,
        name: '东莞',
        percentage: '0%',
    },
    {
        id: 4,
        name: '佛山',
        percentage: '0%',
    },
    {
        id: 5,
        name: '其它',
        percentage: '0%',
    },
])
