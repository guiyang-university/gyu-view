import * as Cesium from 'cesium'

// export default function useMousePosition(viewer: Viewer) {
//     const divDom = document.createElement('div')
//     divDom.style.cssText = `
//     position: fixed;
//     bottom:0;
//     right:0;
//     width:200px;
//     height:40px;
//     background-color: rgba(0,0,0,0.5);
//     color: #fff;
//     font-size: 14px;
//     line-height: 40px;
//     text-align: center;
//     z-index: 100;
//   `
//     document.body.appendChild(divDom)
//     // 监听鼠标移动事件
//     const handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas)
//     handler.setInputAction((movement: {endPosition: Cesium.Cartesian2}) => {
//         // 获取鼠标位置
//         const cartesian = viewer.camera.pickEllipsoid(
//             movement.endPosition,
//             viewer.scene.globe.ellipsoid,
//         )
//         if (cartesian) {
//             // 转换为经纬度
//             const cartographic = Cesium.Cartographic.fromCartesian(cartesian)
//             const longitudeString = Cesium.Math.toDegrees(cartographic.longitude).toFixed(2)
//             const latitudeString = Cesium.Math.toDegrees(cartographic.latitude).toFixed(2)
//             divDom.innerText = `经度：${longitudeString} 纬度：${latitudeString} `
//         }
//     }, Cesium.ScreenSpaceEventType.MOUSE_MOVE)
// }

interface IMoucePosition {
    viewer: Cesium.Viewer
    el?: string
}

export default class MousePosition {
    el: HTMLElement
    viewer: Cesium.Viewer
    divDom: HTMLElement | undefined
    handler: Cesium.ScreenSpaceEventHandler | undefined
    longitude: number
    latitude: number
    constructor(optons: IMoucePosition) {
        this.viewer = optons.viewer
        this.el = optons.el ? document.getElementById(optons.el)! : document.body
        this.divDom
        this.handler
        this.init()
        this.longitude = 0
        this.latitude = 0
    }

    init() {
        this.divDom = document.createElement('div')
        this.divDom.style.cssText = `
          position: absolute;
          bottom:0;
          right:0;
          width:300px;
          height:40px;
          background-color: rgba(0,0,0,0.5);
          color: #fff;
          font-size: 14px;
          line-height: 40px;
          text-align: center;
          z-index: 900;
        `
        this.el.appendChild(this.divDom)
        const viewer = this.viewer
        this.handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas)
        this.handler.setInputAction((movement: {endPosition: Cesium.Cartesian2}) => {
            // 获取鼠标位置
            const cartesian = viewer.camera.pickEllipsoid(
                movement.endPosition,
                viewer.scene.globe.ellipsoid,
            )
            if (cartesian) {
                // 转换为经纬度
                const cartographic = Cesium.Cartographic.fromCartesian(cartesian)
                const longitudeString = Cesium.Math.toDegrees(cartographic.longitude)
                const latitudeString = Cesium.Math.toDegrees(cartographic.latitude)
                this.divDom!.innerText = `经度：${longitudeString.toFixed(
                    5,
                )}  纬度：${latitudeString.toFixed(5)} `

                this.longitude = longitudeString
                this.latitude = latitudeString
            }
        }, Cesium.ScreenSpaceEventType.MOUSE_MOVE)
    }

    getCoordinate() {
        return {
            longitude: this.longitude,
            latitude: this.latitude,
        }
    }

    destroy() {
        this.divDom = undefined
        this.handler?.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE)
        this.handler?.destroy()
    }
}
