/*
 * @Author: Wang Wei 2419715149
 * @Date: 2023-08-29 20:11:30
 * @LastEditors: ww 92363803+lisrmww@users.noreply.github.com
 * @LastEditTime: 2023-09-02 16:03:15
 * @Description: 系统路由，由公开路由表和私有路由表组成
 * 私有路由表按照系统模块划分
 */
import {createRouter, createWebHistory, type RouteRecordRaw} from 'vue-router'
import Layout from '../layout/index.vue'
import SystemRoute from './module/systemManager'

/**
 * 公开路由表
 */
const publicRoutes: RouteRecordRaw[] = [
    {
        path: '/login',
        name: 'Login',
        component: () => import('@/views/public/login/Login.vue'),
    },
    {
        path: '/:pathMatch(.*)*',
        component: () => import('@/views/public/not-found/NotFound.vue'),
    },
]

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [...publicRoutes, ...SystemRoute],
})

export default router
