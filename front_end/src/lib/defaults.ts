import type {Table} from 'element-plus'
import type {ComponentInternalInstance, VNode} from 'vue'

type SearchType = 'input' | 'select'
type Optons = {label: string; key: string; value: string}

interface TableProp {
    label: string
    prop: string
    search?: boolean
    type?: SearchType
    placeholder?: string
    options: Optons[]
}

type ModelType = 'edit' | 'create' | 'importExcel' | 'exportExcel' | 'descriptions'

export type {TableProp, ModelType}
