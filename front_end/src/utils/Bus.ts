//@ts-nocheck
import mitt from 'mitt'
const emitter = mitt()

export default emitter
