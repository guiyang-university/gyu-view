import 'normalize.css'
import 'nprogress/nprogress.css'
import './assets/main.css'

import {createApp} from 'vue'
import {createPinia} from 'pinia'

import App from './App.vue'
import router from './router'
import lib from '@/lib'
import directives from './directives'
import {Icon} from '@iconify/vue'
// 动态设置REM基准值
import 'amfe-flexible'
// import './assets/js/flexible.js'

import 'virtual:customIcon'

const app = createApp(App)

app.use(createPinia())

app.use(router)
app.use(lib)
app.use(directives)

//   <Icons :icon="`custom:${item.icon}`" width="28" height="28" />
app.component('Icon', Icon)

app.mount('#app')
