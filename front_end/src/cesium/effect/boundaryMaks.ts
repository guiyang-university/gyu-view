import * as Cesium from 'cesium'
/**
 * 地图局部遮罩
 */

export default class PartMask {
    viewer: Cesium.Viewer
    constructor(viewer: Cesium.Viewer) {
        this.viewer = viewer
        viewer.scene.sun.show = false
        viewer.scene.moon.show = false
        viewer.scene.skyBox.show = false
        // @ts-ignore
        viewer.scene.undergroundMode = true
        viewer.scene.globe.show = true
        viewer.scene.backgroundColor = new Cesium.Color(0, 0, 0, 0)
        viewer.scene.skyAtmosphere.show = false
        // @ts-ignore
        if (Cesium.FeatureDetection.supportsImageRenderingPixelated()) {
            //判断是否支持图像渲染像素化处理
            viewer.resolutionScale = window.devicePixelRatio
        }
        // @ts-ignore
        viewer.scene.fxaa = true
        viewer.scene.postProcessStages.fxaa.enabled = true
    }

    initBoundary(geojson: any) {
        const viewer = this.viewer
        console.log(geojson.features)
        const boundary = geojson.features[0].geometry.coordinates
        let arr: any = []
        boundary.forEach((item: any) => {
            arr.push(item[0])
            arr.push(item[1])
        })
        let polygon = new Cesium.PolygonGeometry({
            polygonHierarchy: new Cesium.PolygonHierarchy(
                Cesium.Cartesian3.fromDegreesArray([
                    73.0, 53.0, 73.0, 0.0, 135.0, 0.0, 135.0, 53.0,
                ]),
                [new Cesium.PolygonHierarchy(Cesium.Cartesian3.fromDegreesArray(arr))],
            ),
        })

        let polygonInstance = new Cesium.GeometryInstance({
            geometry: polygon,
            attributes: {
                color: Cesium.ColorGeometryInstanceAttribute.fromColor(Cesium.Color.BLACK),
                show: new Cesium.ShowGeometryInstanceAttribute(true),
            },
        })
        viewer.scene.primitives.add(
            new Cesium.GroundPrimitive({
                geometryInstances: polygonInstance,
                appearance: new Cesium.PerInstanceColorAppearance({
                    translucent: true, //false时透明度无效
                    closed: false,
                }),
            }),
        )
        function addRect(
            left: number | undefined,
            down: number | undefined,
            right: number | undefined,
            up: number | undefined,
        ) {
            let polygon = new Cesium.RectangleGeometry({
                rectangle: Cesium.Rectangle.fromDegrees(left, down, right, up),
            })
            let polygonInstance = new Cesium.GeometryInstance({
                geometry: polygon,
                attributes: {
                    color: Cesium.ColorGeometryInstanceAttribute.fromColor(Cesium.Color.BLACK),
                    show: new Cesium.ShowGeometryInstanceAttribute(true),
                },
            })
            viewer.scene.primitives.add(
                new Cesium.GroundPrimitive({
                    geometryInstances: polygonInstance,
                    appearance: new Cesium.PerInstanceColorAppearance({
                        translucent: true, //false时透明度无效
                        closed: false,
                    }),
                }),
            )
        }
        addRect(-180.0, -90.0, 73.0, 90.0)
        addRect(135.0, -90.0, 180.0, 90.0)
        addRect(73.0, 53.0, 135.0, 90.0)
        addRect(73.0, -90.0, 135.0, 0.0)
    }
}
