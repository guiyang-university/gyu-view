package com.gyu.common.utils;

import org.springframework.util.DigestUtils;

/**
 * MD5工具类
 */
public class MD5Utils {
    //盐，用于混交md5
    private static final String slat = "&%5123***&&%%$$#@";

    /**
     * 生成md5
     * @return
     */
    public static String getMD5(String str, boolean isSalt) {
        if (isSalt) {
            str = str + "/" + slat;
        }
        String md5 = DigestUtils.md5DigestAsHex(str.getBytes());
        return md5;
    }

    public static String getCpuMD5(String str) {
        str = str + "/" + slat;
        String md5 = DigestUtils.md5DigestAsHex(str.getBytes());
        return md5;
    }

}



