import * as Cesium from "cesium";

export class RadarScaning {
  viewer: Cesium.Viewer;
  rotation: number;
  entity: Cesium.Entity | undefined;
  constructor(viewer: Cesium.Viewer) {
    this.viewer = viewer;
    this.rotation = Cesium.Math.toRadians(90);

    console.log(this.rotation);

    this.addRadarScaning();

    if (Cesium.defined(this.entity)) {
      viewer.zoomTo(this.entity as Cesium.Entity);
    }
  }

  /**
   * 绘制雷达扫描的圆圈与四分之一的渐变半圆
   */
  drawRadarCanvas() {
    const canvas = document.createElement("canvas");
    canvas.width = 300;
    canvas.height = 300;
    const context = canvas.getContext("2d") as CanvasRenderingContext2D;

    // 绘制圆
    context.lineWidth = 1;
    context.strokeStyle = "red";
    context.arc(150, 150, 140, 0, 360, false);
    context.stroke();
    context.closePath();

    // 绘制渐变半圆
    const grd = context.createLinearGradient(175, 100, canvas.width, 150);
    grd.addColorStop(0, "rgba(255,0,0,0)");
    grd.addColorStop(1, "rgba(255,0,0,0.8)");
    context.fillStyle = grd;
    context.beginPath();
    context.moveTo(150, 150);
    context.arc(150, 150, 140, (-90 / 180) * Math.PI, (0 / 180) * Math.PI);
    context.fill();
    return canvas;
  }

  /**
   * 绘制三维场景的雷达扫描
   */
  addRadarScaning() {
    const radarCanvas = this.drawRadarCanvas();
    //绘制雷达扫描的三维场景的实体
    this.entity = this.viewer.entities.add({
      position: Cesium.Cartesian3.fromDegrees(100.0, 40.0),
      name: "radar",
      ellipse: {
        semiMajorAxis: 3000.0,
        semiMinorAxis: 3000.0,
        material: new Cesium.ImageMaterialProperty({
          image: radarCanvas,
          transparent: true
        }),
        fill: true,
        outline: true,
        outlineColor: Cesium.Color.GREEN,
        outlineWidth: 10,
        rotation: new Cesium.CallbackProperty(this.getRotationValue.bind(this), false),
        stRotation: new Cesium.CallbackProperty(this.getRotationValue.bind(this), false)
      }
    });
  }

  getRotationValue() {
    this.rotation += 0.05;
    return this.rotation;
  }
}
