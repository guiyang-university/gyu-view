package com.gyu.system;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot扫描路径，实例化实体类，配置类的路径
 */
@SpringBootApplication(scanBasePackages = {"com.gyu"})
/**
 * mapper扫描路径
 */
@MapperScan("com.gyu.system.*.mapper")
public class GyuSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(GyuSystemApplication.class, args);
    }

}
