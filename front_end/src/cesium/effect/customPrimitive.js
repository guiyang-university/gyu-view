import * as Cesium from "cesium";
function Sun() {
  this.show = true;

  this._drawCommand = new Cesium.DrawCommand({
    primitiveType: Cesium.PrimitiveType.TRIANGLES,
    boundingVolume: new Cesium.BoundingSphere(),
    owner: this
  });

  this._commands = {
    drawCommand: this._drawCommand,
    computeCommand: undefined
  };
  this._boundingVolume = new Cesium.BoundingSphere();
  this._boundingVolume2D = new Cesium.BoundingSphere();

  this._texture = undefined;
  this._drawingBufferWidth = undefined;
  this._drawingBufferHeight = undefined;
  this._radiusTS = undefined;
  this._size = undefined;

  this.glowFactor = 1.0;
  this._glowFactorDirty = false;

  this._useHdr = undefined;

  const that = this;
  this._uniformMap = {
    u_texture: function () {
      return that._texture;
    },
    u_size: function () {
      return that._size;
    }
  };
}

Object.defineProperties(Sun.prototype, {
  glowFactor: {
    get: function () {
      return this._glowFactor;
    },
    set: function (glowFactor) {
      glowFactor = Math.max(glowFactor, 0.0);
      this._glowFactor = glowFactor;
      this._glowFactorDirty = true;
    }
  }
});

const scratchPositionWC = new Cesium.Cartesian2();
const scratchLimbWC = new Cesium.Cartesian2();
const scratchPositionEC = new Cesium.Cartesian4();
const scratchCartesian4 = new Cesium.Cartesian4();

Sun.prototype.update = function (frameState, passState, useHdr) {
  if (!this.show) {
    return undefined;
  }

  const mode = frameState.mode;
  if (mode === Cesium.SceneMode.SCENE2D || mode === Cesium.SceneMode.MORPHING) {
    return undefined;
  }

  if (!frameState.passes.render) {
    return undefined;
  }

  const context = frameState.context;
  const drawingBufferWidth = passState.viewport.width;
  const drawingBufferHeight = passState.viewport.height;

  if (
    !Cesium.defined(this._texture) ||
    drawingBufferWidth !== this._drawingBufferWidth ||
    drawingBufferHeight !== this._drawingBufferHeight ||
    this._glowFactorDirty ||
    useHdr !== this._useHdr
  ) {
    this._texture = this._texture && this._texture.destroy();
    this._drawingBufferWidth = drawingBufferWidth;
    this._drawingBufferHeight = drawingBufferHeight;
    this._glowFactorDirty = false;
    this._useHdr = useHdr;

    let size = Math.max(drawingBufferWidth, drawingBufferHeight);
    size = Math.pow(2.0, Math.ceil(Math.log(size) / Math.log(2.0)) - 2.0);
    size = Math.max(1.0, size);

    const pixelDatatype = useHdr
      ? context.halfFloatingPointTexture
        ? Cesium.PixelDatatype.HALF_FLOAT
        : Cesium.PixelDatatype.FLOAT
      : Cesium.PixelDatatype.UNSIGNED_BYTE;
    this._texture = new Cesium.Texture({
      context: context,
      width: size,
      height: size,
      pixelFormat: Cesium.PixelFormat.RGBA,
      pixelDatatype: pixelDatatype
    });

    this._glowLengthTS = this._glowFactor * 5.0;
    this._radiusTS = (1.0 / (1.0 + 2.0 * this._glowLengthTS)) * 0.5;

    const that = this;
    const uniformMap = {
      u_radiusTS: function () {
        return that._radiusTS;
      }
    };

    this._commands.computeCommand = new Cesium.ComputeCommand({
      fragmentShaderSource: Cesium.SunTextureFS,
      outputTexture: this._texture,
      uniformMap: uniformMap,
      persists: false,
      owner: this,
      postExecute: function () {
        that._commands.computeCommand = undefined;
      }
    });
  }

  const drawCommand = this._drawCommand;

  if (!Cesium.defined(drawCommand.vertexArray)) {
    const attributeLocations = {
      direction: 0
    };

    const directions = new Uint8Array(4 * 2);
    directions[0] = 0;
    directions[1] = 0;

    directions[2] = 255;
    directions[3] = 0.0;

    directions[4] = 255;
    directions[5] = 255;

    directions[6] = 0.0;
    directions[7] = 255;

    const vertexBuffer = Cesium.Buffer.createVertexBuffer({
      context: context,
      typedArray: directions,
      usage: Cesium.BufferUsage.STATIC_DRAW
    });
    const attributes = [
      {
        index: attributeLocations.direction,
        vertexBuffer: vertexBuffer,
        componentsPerAttribute: 2,
        normalize: true,
        componentDatatype: Cesium.ComponentDatatype.UNSIGNED_BYTE
      }
    ];
    const indexBuffer = Cesium.Buffer.createIndexBuffer({
      context: context,
      typedArray: new Uint16Array([0, 1, 2, 0, 2, 3]),
      usage: Cesium.BufferUsage.STATIC_DRAW,
      indexDatatype: Cesium.IndexDatatype.UNSIGNED_SHORT
    });
    drawCommand.vertexArray = new Cesium.VertexArray({
      context: context,
      attributes: attributes,
      indexBuffer: indexBuffer
    });

    drawCommand.shaderProgram = Cesium.ShaderProgram.fromCache({
      context: context,
      vertexShaderSource: Cesium.SunVS,
      fragmentShaderSource: Cesium.SunFS,
      attributeLocations: attributeLocations
    });

    drawCommand.renderState = Cesium.RenderState.fromCache({
      blending: Cesium.BlendingState.ALPHA_BLEND
    });
    drawCommand.uniformMap = this._uniformMap;
  }

  const sunPosition = context.uniformState.sunPositionWC;
  const sunPositionCV = context.uniformState.sunPositionColumbusView;

  const boundingVolume = this._boundingVolume;
  const boundingVolume2D = this._boundingVolume2D;

  Cartesian3.clone(sunPosition, boundingVolume.center);
  boundingVolume2D.center.x = sunPositionCV.z;
  boundingVolume2D.center.y = sunPositionCV.x;
  boundingVolume2D.center.z = sunPositionCV.y;

  boundingVolume.radius =
    Cesium.CesiumMath.SOLAR_RADIUS + Cesium.CesiumMath.SOLAR_RADIUS * this._glowLengthTS;
  boundingVolume2D.radius = boundingVolume.radius;

  if (mode === Cesium.SceneMode.SCENE3D) {
    Cesium.BoundingSphere.clone(boundingVolume, drawCommand.boundingVolume);
  } else if (mode === SceneMode.COLUMBUS_VIEW) {
    Cesium.BoundingSphere.clone(boundingVolume2D, drawCommand.boundingVolume);
  }

  const position = Cesium.SceneTransforms.computeActualWgs84Position(
    frameState,
    sunPosition,
    scratchCartesian4
  );

  const dist = Cartesian3.magnitude(
    Cartesian3.subtract(position, frameState.camera.position, scratchCartesian4)
  );
  const projMatrix = context.uniformState.projection;

  const positionEC = scratchPositionEC;
  positionEC.x = 0;
  positionEC.y = 0;
  positionEC.z = -dist;
  positionEC.w = 1;

  const positionCC = Matrix4.multiplyByVector(projMatrix, positionEC, scratchCartesian4);
  const positionWC = SceneTransforms.clipToGLWindowCoordinates(
    passState.viewport,
    positionCC,
    scratchPositionWC
  );

  positionEC.x = CesiumMath.SOLAR_RADIUS;
  const limbCC = Matrix4.multiplyByVector(projMatrix, positionEC, scratchCartesian4);
  const limbWC = SceneTransforms.clipToGLWindowCoordinates(passState.viewport, limbCC, scratchLimbWC);

  this._size = Cartesian2.magnitude(Cartesian2.subtract(limbWC, positionWC, scratchCartesian4));
  this._size = 2.0 * this._size * (1.0 + 2.0 * this._glowLengthTS);
  this._size = Math.ceil(this._size);

  return this._commands;
};

Sun.prototype.isDestroyed = function () {
  return false;
};

Sun.prototype.destroy = function () {
  const command = this._drawCommand;
  command.vertexArray = command.vertexArray && command.vertexArray.destroy();
  command.shaderProgram = command.shaderProgram && command.shaderProgram.destroy();

  this._texture = this._texture && this._texture.destroy();

  return destroyObject(this);
};
export default Sun;
