/**
 * 加载贵阳市WMTS瓦片
 */
import * as Cesium from 'cesium'

export function loadGeoServerTileMap() {
    const wgs84 = new Cesium.GeographicTilingScheme()

    // const imageryProvider2 = new Cesium.WebMapTileServiceImageryProvider({
    //     url: 'http://192.168.31.173:8080/geoserver/gwc/service/wmts?layer=guiyang:gysyx&style={style}&tilematrixset=EPSG:4326&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/png&TileMatrix={TileMatrixSet}:{TileMatrix}&TileCol={TileCol}&TileRow={TileRow}',
    //     layer: 'guiyang:gysyx',
    //     style: '',
    //     format: 'image/png',
    //     tileMatrixSetID: 'EPSG:4326',
    //     maximumLevel: 19,
    //     tilingScheme: wgs84,
    // })

    const imageryProvider = new Cesium.WebMapTileServiceImageryProvider({
       // url: 'http://192.168.31.173:8080/geoserver/gwc/service/wmts?layer=GYU:gyu&style={style}&tilematrixset=EPSG:4326&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/png&TileMatrix={TileMatrixSet}:{TileMatrix}&TileCol={TileCol}&TileRow={TileRow}',
        url: 'http://127.0.0.1:8080/geoserver/gwc/service/wmts?layer=GYU:gyu&style={style}&tilematrixset=EPSG:4326&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/png&TileMatrix={TileMatrixSet}:{TileMatrix}&TileCol={TileCol}&TileRow={TileRow}',
        layer: 'GYU:gyu',
        style: '',
        format: 'image/png',
        tileMatrixSetID: 'EPSG:4326',
        maximumLevel: 17,
        tilingScheme: wgs84,
    })

    return [imageryProvider]
}
