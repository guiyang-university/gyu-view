import type {AxiosRequestConfig, AxiosResponse} from 'axios'

/**
 * requestInterceptors: 请求成功拦截器\
 * requestInterceptorsCatch: 请求错误拦截器\
 * responseInterceptors: 响应成功拦截器\
 * responseInterceptorsCatch: 响应错误的拦截器
 */
export interface Interceptors<T = AxiosResponse> {
    requestInterceptors?: (config: any) => any
    requestInterceptorsCatch?: (err: any) => any
    responseInterceptors?: (res: T) => T
    responseInterceptorsCatch?: (err: any) => any
}

/**
 * interceptors: [可选参数]拦截器\
 * isShowLoading [可选参数]进度条
 */
export interface RequestConfig<T = AxiosResponse> extends AxiosRequestConfig {
    interceptors?: Interceptors<T>
    isShowLoading?: boolean
}
