import * as Cesium from 'cesium'
import landmark from '../data/landmark.json'
import location from '@/assets/icon/location.png'
import men from '@/assets/mapmark/men.png'
import tushuguan from '@/assets/mapmark/tushuguan.png'
import xiezilou from '@/assets/mapmark/xiezilou.png'
import tingchecang from '@/assets/mapmark/tingchecang.png'
import xiaoqu from '@/assets/mapmark/xiaoqu.png'
import meishi from '@/assets/mapmark/meishi.png'
import keyanjigou from '@/assets/mapmark/keyanjigou.png'

const mapmark: any = {
    men,
    tushuguan,
    xiezilou,
    tingchecang,
    xiaoqu,
    meishi,
    keyanjigou,
    location,
}

export function addLandmark(viewer: Cesium.Viewer) {
    const landmarkDataSource = new Cesium.CustomDataSource('landmark')
    const features = landmark.features
    features.forEach((point) => {
        const mark = point.properties.mark
        if (mark) {
            const [lang, lon, height] = point.geometry.coordinates
            landmarkDataSource.entities.add({
                name: 'landmark',
                position: Cesium.Cartesian3.fromDegrees(lang, lon, height),
                billboard: {
                    image: mapmark[mark],
                    show: true, //是否显示
                    verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
                    eyeOffset: new Cesium.Cartesian3(0, 0, -50),
                    heightReference: Cesium.HeightReference.NONE, //表示相对于地形的位置。CLAMP_TO_GROUND	位置固定在地形上。RELATIVE_TO_GROUND	位置高度是指地形上方的高度。
                    //color: Cesium.Color.YELLOW.withAlpha(0.8), //指定图像的颜色
                    width: 32, //广告牌的宽度，覆盖之前默认的像素值
                    height: 32, //广告牌的高度，覆盖之前默认的像素值
                    scaleByDistance: new Cesium.NearFarScalar(500, 1, 10000, 0.0), //设置广告牌的近距离和远距离缩放属性
                    //translucencyByDistance:new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6,0.0),//根据距摄像机的距离来指定广告牌的透明度
                    //pixelOffsetScaleByDistance:new Cesium.NearFarScalar(1.5e2, 20, 8.0e6,0.0),//根据距照相机的距离指定广告牌的像素偏移
                    distanceDisplayCondition: new Cesium.DistanceDisplayCondition(10.0, 20000.0), //根据与相机的与广告牌远近确定可见性
                    // disableDepthTestDistance: Number.POSITIVE_INFINITY, //获取或设置与相机的距离，在深度处禁用深度测试,Number.POSITIVE_INFINITY无穷大，不会应用深度测试，0始终应用深度测试，应用深度测试避免地形的遮挡
                },
                label: {
                    text: point.properties.name,
                    font: '13px Source Han Sans CN',
                    fillColor: Cesium.Color.RED,
                    outlineColor: Cesium.Color.WHEAT,
                    style: Cesium.LabelStyle.FILL_AND_OUTLINE,
                    pixelOffset: new Cesium.Cartesian2(12, -12), //偏移量
                    eyeOffset: new Cesium.Cartesian3(0, 0, -50),
                    verticalOrigin: Cesium.VerticalOrigin.CENTER, //垂直位置
                    horizontalOrigin: Cesium.HorizontalOrigin.LEFT, //水平位置
                    distanceDisplayCondition: new Cesium.DistanceDisplayCondition(50.0, 2000.0), //根据与相机的与广告牌远近确定可见性
                    //scaleByDistance: new Cesium.NearFarScalar(500, 1, 10000, 0.0), //设置广告牌的近距离和远距离缩放属性
                    disableDepthTestDistance: 500,
                },
            })
        }
    })

    viewer.dataSources.add(landmarkDataSource)

    const handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas)
    handler.setInputAction(function (event: any) {
        let pickedFeature = viewer.scene.pick(event.position)
        if (!Cesium.defined(pickedFeature)) {
            return
        }
        let entity = pickedFeature.id
        if (Cesium.defined(entity)) {
            console.log(entity)
        }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK)
}
