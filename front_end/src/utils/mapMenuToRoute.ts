import type {RouteRecordRaw} from 'vue-router'
import {usePermissionStore} from '@/stores/permission'

/**
 * 将views/private文件夹下的 vue 组件生成对应的路由
 * @returns 路由
 */
export function mapViewToRoutes() {
    const routeConfig: Record<string, any> = import.meta.glob('../views/private/**/*.vue', {
        eager: true,
        import: 'routeConfig',
    })

    const components = import.meta.glob('../views/private/**/*.vue')

    const routes = Object.entries(routeConfig).map(([path, config]) => {
        // 路由name
        const arr = path.split('/')
        const routeName = arr[arr.length - 1].replace('.vue', '')
        // 路由path
        let routePath = path.replace('../views/private', '').replace('.vue', '') || '/'
        // 将类似 /aaaa/aaaa 的path处理成 /aaaa 的形式
        const pathArr = routePath.split('/')
        if (pathArr[pathArr.length - 1] === pathArr[pathArr.length - 2]) {
            routePath = path.replace('../views/private', '').replace('/' + routeName + '.vue', '')
        }

        return {
            path: routePath,
            name: routeName,
            meta: config,
            component: components[path],
        } as RouteRecordRaw
    })
    console.log(routes)
    return routes
}

/**
 * 根据用户的权限菜单筛选出相应的路由
 * @param userMenus
 * @returns
 */
export function mapMenusToRoutes(userMenus?: any[]): RouteRecordRaw[] {
    const routes: RouteRecordRaw[] = []
    const allRoutes = mapViewToRoutes()

    const permissionStore = usePermissionStore()

    // 递归获取需要添加的route
    const _recurseGetRouter = (menus: any) => {
        for (const menu of menus) {
            if (menu.path) {
                const route = allRoutes.find((route) => {
                    if (route.path === menu.path) {
                        if (menu.children) {
                            route.children
                        }
                        return true
                    }
                })
                if (route) routes.push(route)
            }
            if (menu?.children?.length > 0) {
                _recurseGetRouter(menu.children)
            }
        }
    }

    // _recurseGetRouter(userMenus)
    _recurseGetRouter(permissionStore.userMenus)

    return routes
}
