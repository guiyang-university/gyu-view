/**
 * 将指定文件夹中的png、jpg格式的图标转换成为css背景图
 *
 * 1、图片转换为base64
 */
import type {PluginOption} from "vite";

const reader = new FileReader();

export default function (): PluginOption {
  const virtualModuleId = "virtual:imageIcon";
  const resolvedVirtualModuleId = "\0" + virtualModuleId;

  return {
    name: "vite-image-to-css",
    resolveId(id) {
      if (id === virtualModuleId) {
        return resolvedVirtualModuleId;
      }
    },
    async load(id) {
      if (id === resolvedVirtualModuleId) {
      }
    }
  };
}

/**
 * 加载指定文件夹中的png、jpg图片
 */
function loadImageIcons() {}

/**
 * 将图片转换为base64格式
 */
function convertImageToBase64() {}

/**
 * 生成css
 */
function generatorCss() {}

// .menu-icon {
//     display: inline-block;
//     width: 25px;
//     height: 25px;
//     background-position: center;
//     background-repeat: no-repeat;
//     background-position: left top;
//     background-size: 100%;
//     margin-right: 5px;
//   }
//   .home {
//     background-image: url("~@/assets/nav/home.png");
//   }
