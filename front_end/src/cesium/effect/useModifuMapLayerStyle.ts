import type { Viewer } from 'cesium'
// 修改地图底色
export default function useModifuMapLayerStyle(viewer: Viewer) {
  //   const baseLayer = viewer.imageryLayers.get(0)
  // 是否进行颜色的翻转
  const inverColor = true
  // 颜色过滤
  const filterRGB: [number, number, number] = [0, 50, 100]
  // 更改底图着色器
  const globe: any = viewer.scene.globe
  const baseFragmentShader = globe._surfaceShaderSet.baseFragmentShaderSource.sources

  for (let i = 0, len = baseFragmentShader.length; i < len; i++) {
    const strS = 'color = czm_saturation(color, textureSaturation);\n#endif\n'
    let strT = 'color = czm_saturation(color, textureSaturation);\n#endif\n'

    if (inverColor) {
      strT += `
        color.r = 1.0 - color.r;
        color.g = 1.0 - color.g;
        color.b = 1.0 - color.b;
      `
    }
    if (filterRGB) {
      strT += `
        color.r = color.r*${filterRGB[0]}.0/255.0;
        color.g = color.g*${filterRGB[1]}.0/255.0;
        color.b = color.b*${filterRGB[2]}.0/255.0;
      `
    }

    baseFragmentShader[i] = baseFragmentShader[i].replace(strS, strT)
  }
}
