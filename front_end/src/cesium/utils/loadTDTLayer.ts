function createTdtImageryProvider(params) {
    var tileMatrixSet = 'w'
    var host = params.host || 'http://t{s}.tianditu.com/'
    var subdomains = ['0', '1', '2', '3', '4', '5', '6', '7']

    if (host[host.length - 1] == '/') {
        host = host.substr(0, host.length - 1)
    }
    var url =
        host +
        '/' +
        params.layer +
        '_' +
        tileMatrixSet +
        '/wmts?service=wmts&request=GetTile&version=1.0.0&LAYER=' +
        params.layer +
        '&tileMatrixSet=' +
        tileMatrixSet +
        '&TileMatrix={TileMatrix}&TileRow={TileRow}&TileCol={TileCol}&style=default&format=tiles'
    url += '&tk=' + params.appKey

    let provider = new Cesium.WebMapTileServiceImageryProvider({
        url: url,
        layer: params.layer,
        style: 'default',
        subdomains: subdomains,
        tileMatrixSetID: tileMatrixSet,
        maximumLevel: params.maximumLevel || 18,
        minimumLevel: params.minimumLevel,
    })

    return provider
}
var imageryProvider = createTdtImageryProvider({
    layer: 'vec',
    appKey: '你的天地图AppKey',
})
//接下来我们就要对这个图层进行处理
var layer = viewer.imageryLayers.addImageryProvider(imageryProvider)
