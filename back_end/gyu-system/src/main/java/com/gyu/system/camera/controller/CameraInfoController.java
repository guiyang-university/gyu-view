package com.gyu.system.camera.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gyu.common.domain.CameraInfo;
import com.gyu.common.response.ApiResult;
import com.gyu.system.camera.service.CameraInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2023-12-25
 */
@RestController
@RequestMapping("/camera/cameraInfo")
public class CameraInfoController {

    @Autowired
    private CameraInfoService cameraInfoService;

    /**
     * 获取摄像头列表
     * @return
     */
    @GetMapping("/list")
    public ApiResult getCameraList(@RequestParam(value = "cameraName", required = false) String cameraName,
                                   @RequestParam(value = "cameraCode", required = false) String cameraCode,
                                   @RequestParam(value = "cameraLocation", required = false) String cameraLocation,
                                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                   @RequestParam(value = "pageSize", defaultValue = "8") Integer pageSize){
        IPage pageInfo = cameraInfoService.findCameraByPage(cameraName, cameraCode, cameraLocation, pageNum, pageSize);
        if (null != pageInfo) {
            return  new ApiResult().success(pageInfo);
        }
        return new ApiResult().error();
    }

    /**
     * 获取所有摄像头
     * @return
     */
    @GetMapping("/cameras")
    public ApiResult getAllCameraList(){
        List<CameraInfo> all = cameraInfoService.findAll();
        if (null != all) {
            return  new ApiResult().success(all);
        }
        return new ApiResult().error();
    }


    /**
     * 新增摄像头
     * @return
     */
    @PostMapping("/create")
    public ApiResult createCamera(@RequestBody CameraInfo cameraInfo){
        boolean bool = cameraInfoService.createCamera(cameraInfo);

        if (bool){
            return new ApiResult().success(bool);
        }

        return new ApiResult().error();
    }

    @PostMapping("/update")
    public ApiResult updateCamera(@RequestBody CameraInfo cameraInfo){
        boolean bool = cameraInfoService.updateCameraById(cameraInfo);
        if (bool){
            return new ApiResult().success(bool);
        }
        return new ApiResult().error();
    }

    /**
     * 根据ids批量删除摄像头
     * @return
     */
    @PostMapping("/delete")
    public ApiResult deleteCameraById(@RequestBody CameraInfo cameraInfo){

        List<String> ids = cameraInfo.getIds();


        if(null != ids){
            boolean bool = cameraInfoService.deleteCameraById(ids);
            if (bool){
                return new ApiResult().success(bool);
            }
        }
        return new ApiResult().error();
    }

}
