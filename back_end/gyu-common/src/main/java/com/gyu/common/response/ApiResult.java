package com.gyu.common.response;


import com.gyu.common.enums.BizCodeEnum;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ApiResult<T> {
    /**
     * 响应的状态码
     */
    private Integer code;
    /**
     * 响应的状态信息
     */
    private String message;
    /**
     * 响应的数据结果
     */
    private T data;

    public static <T> ApiResult<T> success() {
        return builder(BizCodeEnum.SUCCESS.getCode(), BizCodeEnum.SUCCESS.getMsg(), null);
    }

    public static <T> ApiResult<T> success(T data) {
        return builder(BizCodeEnum.SUCCESS.getCode(), BizCodeEnum.SUCCESS.getMsg(), data);
    }

    public static <T> ApiResult<T> success(T data, String message) {
        return builder(BizCodeEnum.SUCCESS.getCode(), message, data);
    }

    public static <T> ApiResult<T> error() {
        return builder(BizCodeEnum.ERROR.getCode(), BizCodeEnum.ERROR.getMsg(), null);
    }

    public static <T> ApiResult<T> error(Integer code, String message) {
        return builder(code, message, null);
    }

    public static <T> ApiResult<T> error(BizCodeEnum bizCodeEnum) {
        return builder(bizCodeEnum.getCode(), bizCodeEnum.getMsg(), null);
    }

    public static <T> ApiResult<T> error(String message) {
        return builder(BizCodeEnum.ERROR.getCode(), message, null);
    }


    private static <T> ApiResult<T> builder(Integer code, String message, T data) {
        ApiResult<T> result = new ApiResult();
        result.setCode(code);
        result.setMessage(message);
        result.setData(data);
        return result;
    }
}
