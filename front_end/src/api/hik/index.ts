import request from '..'

const BASE_URL = '/camera/cameraInfo/'

// 园区卡口车辆违章事件查询
export function CarIllegalEvents(data = {}) {
    return request.post({
        baseURL: '/gyu',
        url: '/guiyangcollege/mon/illegalEvents/illegalEventsPage',
        data: data,
    })
}

// 车辆态势感知
export function getCarInfoSituationalAwareness() {
    return request.get({
        baseURL: '/gyu',
        url: '/guiyangcollege/mon/illegalEvents/getCarInfoSituationalAwareness',
    })
}

// 安全事件总次数
export function getSecurityIncidentNumber(params: any) {
    return request.get({
        baseURL: '/gyu',
        url: '/guiyangcollege/mon/illegalEvents/getSecurityIncidentNumber',
        params,
    })
}

// 摄像头告警情况
export function getRiskEarlyWarningList(params: any) {
    return request.get({
        baseURL:'/gyu',
        url:'/guiyangcollege/mon/statistic/getRiskEarlyWarningList',
        params
    })
}