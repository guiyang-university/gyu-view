import * as Cesium from 'cesium'

type Options = {
    name?: string
    bgColor?: Cesium.Color
    alpha?: number
    invert?: boolean
    preMultiplyAlpha?: boolean
}

const {defaultValue, Color} = Cesium
/**
 * 修改瓦片主题
 */
class ImageryThemeGL {
    params: Options
    constructor(options: Options) {
        this.params = {}

        const params = this.params
        params.bgColor = defaultValue(options.bgColor, Color.TRANSPARENT)
        params.alpha = defaultValue(options.alpha, 1)
        params.invert = defaultValue(options.invert, false)
        params.preMultiplyAlpha = defaultValue(options.preMultiplyAlpha, true)
    }
}
