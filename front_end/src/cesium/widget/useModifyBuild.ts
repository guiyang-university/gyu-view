import * as Cesium from 'cesium'
import type { Viewer } from 'cesium'

export default function useModifyBuild(viewer: Viewer) {
  // 添加3D建筑
  const tiles3d = Cesium.createOsmBuildings()
  viewer.scene.primitives.add(tiles3d)

  tiles3d.style = new Cesium.Cesium3DTileStyle({
    show: "${feature['name']} !== '广州塔'",
  })

  // 监听瓦片加载的事件
  tiles3d.tileVisible.addEventListener(function (tile) {
    const cesium3DTilCon = tile.content
    const featuresLength = cesium3DTilCon.featuresLength

    for (let i = 0; i < featuresLength; i++) {
      const model = cesium3DTilCon.getFeature(i).content._model
      // 修改模型的片元着色器
      model._rendererResources.sourceShaders[1] = `
        varying vec3 v_positionEC;

        void main()
        {
            czm_materialInput materialInput;
            // 获取模型的positon信息
            vec4 position = czm_inverseModelView * vec4(v_positionEC, 1.0);
            float strength = position.z/300.0;
            gl_FragColor = vec4(strength,0.3*strength,strength,1.0);

            // 动态光环
            // czm_frameNumber 获取当前的帧数
            // fract(x),返回x的小数部分
            float time = fract(czm_frameNumber/(60.0*10.0));
            // 实现往返的操作
            time = abs(time-0.5)*2.0;
            // float time  = fract(czm_frameNumber/60.0)*6.28;
            // time = sin(time);
            // clamp(x, min, max)，返回x在min和max之间的最小值
            float diff = abs(clamp(position.z/500.0, 0.0, 1.0) - time);
            diff = step(0.01, diff);
            gl_FragColor.rgb += vec3(0.5)*(1.0-diff);
        }
      `
      // 片元着色器已经修改，需要更新
      model._shouldRegenerateShaders = true
    }
  })
}
