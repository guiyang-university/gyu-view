import * as Cesium from 'cesium'

export default class PerformanceDisplay {
    _lastFpsSampleTime: number
    _fpsFrameCount: number
    _fpsText: Text
    constructor() {
        this._lastFpsSampleTime = Cesium.getTimestamp()
        this._fpsFrameCount = 0
        this._fpsText = document.createTextNode('')
    }

    update(container: HTMLElement) {
        const time = Cesium.getTimestamp()

        this._fpsFrameCount++
        const fpsElapsedTime = time - this._lastFpsSampleTime

        if (fpsElapsedTime > 1000) {
            let fps = ((this._fpsFrameCount * 1000) / fpsElapsedTime) | 0

            this._fpsText.nodeValue = `● ${fps} FPS`
            this._lastFpsSampleTime = time
            this._fpsFrameCount = 0

            if (fps < 60) {
                container.style.color = '#ff0'
            } else if (fps < 30) {
                container.style.color = '#f00'
            } else {
                container.style.color = '#0f0'
            }

            container.appendChild(this._fpsText)
        }
    }

    destroy() {
        return Cesium.destroyObject(this)
    }
}
