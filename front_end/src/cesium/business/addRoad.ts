import * as Cesium from 'cesium'
import roadData from '../data/road.json'
// import PolylineTrailMaterialProperty from '../effect/material/PolylineTrailMaterialProperty'
import ShuttleLineMaterialProperty from '../effect/material/ShuttleLineMaterialProperty'
import road from '../../assets/image/road.png'

export function addRoad(viewer: Cesium.Viewer) {
    Cesium.GeoJsonDataSource.load(roadData, {
        clampToGround: true,
        strokeWidth: 3,
    }).then((dataSource) => {
        let entities = dataSource.entities.values
        let color = new Cesium.Color(0.7, 1.0, 0.7, 1.0)
        let polylineTrailMaterialProperty = new ShuttleLineMaterialProperty(2000, road)

        entities.forEach((item) => {
            let polyline = item.polyline
            // @ts-ignore
            polyline.material = polylineTrailMaterialProperty
        })

        viewer.dataSources.add(dataSource)
    })
}
