// @ts-nocheck
import * as Cesium from 'cesium'
import gsap from 'gsap'

export default class PolylineTrailMaterialProperty {
    constructor(options?: any) {
        options = Cesium.defaultValue(options, Cesium.defaultValue.EMPTY_OBJECT)
        this._definitionChanged = new Cesium.Event()
        this.time = new Date().getTime()
        this.color = new Cesium.Color(0.7, 0.6, 1.0, 1.0)
        this.duration = 1000
    }
}

Object.defineProperties(PolylineTrailMaterialProperty.prototype, {
    isConstant: {
        get: function () {
            return (
                Cesium.Property.isConstant(this.color) && Cesium.Property.isConstant(this.duration)
            )
        },
    },

    definitionChanged: {
        get: function () {
            return this._definitionChanged
        },
    },

    color: Cesium.createPropertyDescriptor('color'),
})

PolylineTrailMaterialProperty.prototype.getType = function (time) {
    return 'PolylineTrailMaterial'
}

PolylineTrailMaterialProperty.prototype.getValue = function (time, result) {
    if (!Cesium.defined(result)) {
        result = {}
    }
    result.color = this.color
    result.time = ((new Date().getTime() - this.time) % this.duration) / this.duration
    console.log('object', result)
    return result
}

PolylineTrailMaterialProperty.prototype.equals = function (other) {
    return (
        this === other ||
        (other instanceof PolylineTrailMaterialProperty &&
            this._color.equals(other._color) &&
            this._time === other._time &&
            this._duration === other._duration)
    )
}

Cesium.Material._materialCache.addMaterial('PolylineTrailMaterial', {
    fabric: {
        type: Cesium.Material.PolylineTrailMaterialType,
        uniforms: {
            uTime: 0,
            uColor: new Cesium.Color(1.0, 0.0, 0.0, 0.0),
        },
        source: `
        czm_material czm_getMaterial(czm_materialInput materialInput)
        {
          // 生成默认的基础材质
          czm_material material = czm_getDefaultMaterial(materialInput);
          // 获取st
          vec2 st = materialInput.st;
          // 获取当前帧数,10秒内变化从0-1；
          float time = fract(czm_frameNumber / (60.0*10.0));
          time = time * (1.0 + 0.1);
          // 平滑过渡函数
          // smoothstep(edge0, edge1, value);
          // 参数1：边缘0,==8,
          // 参数2：边缘1,==10,
          // 参数3：当前值,==7 , result = 0
          // 参数3：当前值,==9 , result = 0.5
          // 参数3：当前值,==10 , result = 1
          float alpha = smoothstep(time-0.1,time, st.s) * step(-time,-st.s);
          alpha += 0.05;
          // 设置材质的透明度
          material.alpha = alpha;
          material.diffuse = uColor.rgb;
          
          return material;
        }
        `,
    },
})
