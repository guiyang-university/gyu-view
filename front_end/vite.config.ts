import {fileURLToPath, URL} from 'node:url'
import path from 'node:path'

import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'

// auto import
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import {ElementPlusResolver} from 'unplugin-vue-components/resolvers'

import Cesium from 'vite-plugin-cesium'

import Icons from 'unplugin-icons/vite'
import {FileSystemIconLoader} from 'unplugin-icons/loaders'
import ViteIconifyBundle from './plugins/vite-iconify-bundle'
import IconsResolver from 'unplugin-icons/resolver'

const pathSrc = path.resolve(__dirname, 'src')
// https://vitejs.dev/config/
export default defineConfig(({mode}) => {
    return {
        plugins: [
            vue(),
            vueJsx(),
            // auto import
            AutoImport({
                resolvers: [
                    ElementPlusResolver(),
                    IconsResolver({prefix: 'Icon'}), // 自动导入图标组件
                ],
                dts: path.resolve(pathSrc, 'auto-imports.d.ts'),
            }),
            Components({
                resolvers: [
                    ElementPlusResolver(),
                    // 查看 https://iconify.design/
                    // 使用方式 {前缀}-{图标集合}-{图标名}
                    // <i-ep-edit />
                    // <i-mdi-account-box style="font-size: 2em; color: red"/>
                    // <el-icon><i-ep-menu /></el-icon>
                    // <Icon icon="ep:menu" />
                    // IconsResolver({ prefix: 'icon', enabledCollections: ['ep'] }), // 默认自动注册 element plus 图标组件
                    IconsResolver({enabledCollections: ['ep']}),
                ],
                dts: path.resolve(pathSrc, 'components.d.ts'),
            }),
            Cesium(),
            Icons({
                compiler: 'vue3',
                customCollections: {
                    my: FileSystemIconLoader('./src/assets/svg', (svg) =>
                        svg.replace(/^<svg /, '<svg fill="currentColor" '),
                    ),
                },
                iconCustomizer(collection, icon, props) {
                    // customize all icons in this collection
                    if (collection === 'my') {
                        props.width = '4em'
                        props.height = '4em'
                    }
                    // customize this icon in this collection
                    if (collection === 'my-icons' && icon === 'account') {
                        props.width = '6em'
                        props.height = '6em'
                    }
                    // customize this @iconify icon in this collection
                    if (collection === 'mdi' && icon === 'account') {
                        props.width = '2em'
                        props.height = '2em'
                    }
                },
            }),
            ViteIconifyBundle({
                // 打包自定义svg图标
                svg: [
                    {
                        dir: path.resolve(__dirname, './src/assets/svg'),
                        monotone: false,
                        prefix: 'custom',
                    },
                ],

                // 从 iconify 打包 icon 集合
                // icons: [
                //   "mdi:home",
                //   "mdi:account",
                //   "mdi:login",
                //   "mdi:logout",
                //   "octicon:book-24",
                //   "octicon:code-square-24"
                // ]

                // 自定义 JSON 文件
                // json: [
                //   // Custom JSON file
                //   // 'json/gg.json',
                //   // Iconify JSON file (@iconify/json is a package name, /json/ is directory where files are, then filename)
                //   require.resolve('@iconify/json/json/tabler.json'),
                //   // Custom file with only few icons
                //   {
                //     filename: require.resolve('@iconify/json/json/line-md.json'),
                //     icons: ['home-twotone-alt', 'github', 'document-list', 'document-code', 'image-twotone'],
                //   },
                // ],
            }),
        ],
        resolve: {
            alias: {
                '@': fileURLToPath(new URL('./src', import.meta.url)),
            },
        },
        build: {
            terserOptions: {
                compress: {
                    drop_console: true, // 生产环境移除conlose
                },
            },
            outDir: 'dist',
            assetsDir: 'assets',
            commonjsOptions: {
                include: /node_modules|lib/,
            },
        },
        server: {
            port: 3000,
            host: '0.0.0.0',
            open: false,
            https: false,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
            proxy: {
                // 本地
                '/api': {
                    target: 'http://127.0.0.1:8989',
                    changeOrigin: true,
                    rewrite: (path) => path.replace(/^\/api/, ''),
                },
                //
                '/gyu': {
                    // target: 'http://172.31.200.216:8899',
                    target: 'http://10.0.135.5:8899',
                    changeOrigin: true,
                    rewrite: (path) => path.replace(/^\/gyu/, ''),
                },
                '/hik': {
                    target: 'https://172.31.200.250:443/artemis',
                    changeOrigin: true,
                    rewrite: (path) => path.replace(/^\/hik/, ''),
                },
            },
        },
    }
})
