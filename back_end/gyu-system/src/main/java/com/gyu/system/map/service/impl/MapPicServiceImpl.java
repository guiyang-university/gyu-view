package com.gyu.system.map.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gyu.common.domain.MapPic;
import com.gyu.common.exception.BizException;
import com.gyu.common.utils.SnowFlakeIdWorker;
import com.gyu.system.map.mapper.MapPicMapper;
import com.gyu.system.map.service.MapPicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2023-12-27
 */
@Service
public class MapPicServiceImpl extends ServiceImpl<MapPicMapper, MapPic> implements MapPicService {

    @Autowired
    private SnowFlakeIdWorker snowFlakeIdWorker;

    @Autowired
    private MapPicMapper mapPicMapper;

    @Override
    public boolean saveMaPic(MapPic mapPic) {
        String mapName = mapPic.getMapName();
        if (StringUtils.isNotEmpty(mapName)) {
            MapPic mapPicByMapName = this.findOneMapPicByMapName(mapName);
            if (mapPicByMapName != null){
                throw new BizException(500, "地图名称重复");
            }
        }

        if (mapPic != null){
            String id = snowFlakeIdWorker.getId();
            mapPic.setId(id);
            boolean save = this.save(mapPic);
            return save;
        }

        return false;
    }

    @Override
    public IPage findMapByPage(String mapName, String mapType, Integer pageNum, Integer pageSize) {
        IPage<MapPic> page = new Page<>(pageNum,pageSize);
        QueryWrapper<MapPic> wrapper = new QueryWrapper<>();

        if (StringUtils.isNotEmpty(mapName)) {
            wrapper.or().like("map_name", mapName);
        }
        if (StringUtils.isNotEmpty(mapType)){
            wrapper.or().like("map_type", mapType);
        }

        IPage<MapPic> mapPicIPage = this.page(page, wrapper);

        return mapPicIPage;
    }

    @Override
    public List<MapPic> findAll() {
        return mapPicMapper.selectList(null);
    }

    public MapPic findOneMapPicByMapName(String mapName){
        QueryWrapper<MapPic> wrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(mapName)) {
            wrapper.eq("map_name", mapName);
            MapPic mapPic = this.getOne(wrapper);
            return mapPic;
        }
        return null;
    }
}
