package com.gyu.system.map.mapper;

import com.gyu.common.domain.MapPic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-12-27
 */
public interface MapPicMapper extends BaseMapper<MapPic> {

}
