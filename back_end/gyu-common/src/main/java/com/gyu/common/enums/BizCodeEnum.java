package com.gyu.common.enums;

/**
 * 业务代码
 */
public enum BizCodeEnum {

    SUCCESS(200, "成功"),

    ERROR(500, "失败"),

    LOGIN_IS_OVERDUE(501, "登录已失效"),

    USER_LOGIN_FAILED(502, "登录已失败"),

    USER_ACCOUNT_NOT_EXISTS(503, "账号不存在"),

    USER_PASSWORD_MISTAKE(504, "密码错误"),

    USER_IS_EXISTS(505,"用户名已存在"),

    RESIDENCE_IS_HOLDER(506,"户主不能删除");

    BizCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private Integer code;
    private String msg;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
