package com.gyu.common.exception;

import com.gyu.common.response.ApiResult;
import com.gyu.common.enums.BizCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 自定义异常
     */
    @ExceptionHandler(BizException.class)
    private ApiResult<Object> handlerServiceException(BizException e) {
        log.error("BizException->>>>>>>> 业务异常: [code(错误码)=" + e.getCode() + ",message(描述)=" + e.getMessage() + "]", e);
        return ApiResult.error(e.getCode(), e.getMessage());
    }


    @ExceptionHandler(Exception.class)
    public ApiResult<Object> handlerException(Exception e) {
        log.error("Exception->>>>>>>> 未知异常: [" + e.getMessage() + "]", e);
        return ApiResult.error(BizCodeEnum.ERROR.getCode(), e.getMessage());
    }

}