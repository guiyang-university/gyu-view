package com.gyu.system.config;

import io.minio.*;
import io.minio.http.Method;
import io.minio.messages.DeleteError;
import io.minio.messages.DeleteObject;
import io.minio.messages.Item;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class MinIoUtils {
    @Autowired
    private MinioClient minioClient;

    @Value("${minio.endpoint}")
    private String endpoint;

    /**
     * 判断bucket是否存在
     *
     * @param bucketName 桶名称
     * @return boolean
     */
    public boolean existBucket(String bucketName) {
        try {
            return minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
        } catch (Exception e) {
            log.error("Method existBucket calling failed ", e);
            return false;
        }
    }

    /**
     * 创建存储bucket
     *
     * @param bucketName 存储bucket名称
     * @return Boolean
     */
    public boolean makeBucket(String bucketName) {
        try {
            minioClient.makeBucket(MakeBucketArgs.builder()
                    .bucket(bucketName)
                    .build());
            return true;
        } catch (Exception e) {
            log.error("Create bucket [{}] failed", bucketName, e);
            return false;
        }
    }

    /**
     * 删除存储bucket
     *
     * @param bucketName 存储bucket名称
     * @return Boolean
     */
    public Boolean removeBucket(String bucketName) {
        try {
            minioClient.removeBucket(RemoveBucketArgs.builder()
                    .bucket(bucketName)
                    .build());
        } catch (Exception e) {
            log.error("Remove bucket [{}] failed", bucketName, e);
            return false;
        }
        return true;
    }

    /**
     * 获取存储桶策略
     *
     * @param bucketName 存储桶名称
     * @return json
     */
    public String getBucketPolicy(String bucketName) {
        try {
            String bucketPolicy = minioClient.getBucketPolicy(GetBucketPolicyArgs.builder().bucket(bucketName).build());
            return bucketPolicy;
        } catch (Exception e) {
            log.error("Get policy of bucket [{}] failed.", bucketName);
            return null;
        }
    }

    /**
     * 设置存储桶策略
     *
     * @param bucketName 存储桶名称
     * @return json
     */
    public void setBucketPolicy(String bucketName, String policy) {
        try {
            minioClient.setBucketPolicy(SetBucketPolicyArgs.builder().bucket(bucketName).config(policy).build());
        } catch (Exception e) {
            log.error("Get policy of bucket [{}] failed.", bucketName);
        }
    }

    /**
     * 上传文件
     *
     * @param multipartFile 文件传输对象
     * @param bucketName    桶名称
     * @return 返回上传成功的文件名
     */
    public String upload(MultipartFile multipartFile, String bucketName) {
        String fileName = multipartFile.getOriginalFilename();
        String[] split = fileName.split("\\.");
        //重置文件名
        if (split.length > 1) {
            fileName = split[0] + "_" + System.currentTimeMillis() + "." + split[1];
        } else {
            fileName = fileName + System.currentTimeMillis();
        }
        try (InputStream in = multipartFile.getInputStream()) {
            //上传文件到minio服务器
            minioClient.putObject(PutObjectArgs.builder()
                    .bucket(bucketName)
                    .object(fileName)
                    .stream(in, in.available(), -1)
                    .contentType(multipartFile.getContentType())
                    .build()
            );
            //拼接文件上传成功后的路径
            String fileUrl = endpoint + "/" + bucketName + "/" + fileName;
            return fileUrl;
        } catch (Exception e) {
            log.error("File [{}] upload to {} failed. ", fileName, bucketName, e);
        }

        return null;
    }

    /**
     * 下载文件
     *
     * @param fileName 下载文件名
     * @return 返回http响应体
     */
    public ResponseEntity<byte[]> download(String bucketName, String fileName) {
        ResponseEntity<byte[]> responseEntity = null;
        try (InputStream in = minioClient.getObject(GetObjectArgs.builder().bucket(bucketName).object(fileName).build());
             FastByteArrayOutputStream out = new FastByteArrayOutputStream();
        ) {
            IOUtils.copy(in, out);
            //封装返回值
            byte[] bytes = out.toByteArray();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
            headers.setContentLength(bytes.length);
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            headers.setAccessControlExposeHeaders(Collections.singletonList("*"));
            responseEntity = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Download file [{}] from bucket [{}] failed.", fileName, bucketName, e);
        }
        return responseEntity;
    }

    /**
     * 非递归遍历bucket, 返回对象及大小
     *
     * @param bucketName 存储bucket名称
     * @return 存储bucket内文件对象信息
     */
    public List<ObjectItem> listObjects(String bucketName, String prefix) {
        Iterable<Result<Item>> results = minioClient.listObjects(ListObjectsArgs.builder().bucket(bucketName).prefix(prefix).recursive(false).build());
        List<ObjectItem> objectItems = new ArrayList<>();
        try {
            for (Result<Item> result : results) {
                Item item = result.get();
                if (item.isDir()) {
                    String objectName = item.objectName();
                    objectName = prefix + objectName;
                    List<ObjectItem> dirObjects = listObjects(bucketName, objectName);
                    if (dirObjects != null && dirObjects.size() > 0)
                        objectItems.addAll(dirObjects);
                } else {
                    ObjectItem objectItem = new ObjectItem();
                    objectItem.setObjectName(item.objectName());
                    objectItem.setSize(item.size());
                    objectItems.add(objectItem);
                }
            }
        } catch (Exception e) {
            log.error("List bucket [{}] failed. ", bucketName, e);
            return null;
        }
        return objectItems;
    }

    /**
     * 批量删除文件对象
     *
     * @param bucketName 存储bucket名称
     * @param objects    对象名称集合
     */
    public boolean removeObjects(String bucketName, List<String> objects) {
        try {
            List<DeleteObject> dos = objects.stream().map(DeleteObject::new).collect(Collectors.toList());
            Iterable<Result<DeleteError>> results = minioClient.removeObjects(RemoveObjectsArgs.builder().bucket(bucketName).objects(dos).build());
            return true;
        } catch (Exception e) {
            log.error("Remove objects from [{}] failed.", bucketName);
            return false;
        }
    }

    /**
     * @param bucketName    桶名称
     * @param objectName    文件对象名称
     * @param expireSeconds url有效期
     * @return 返回get下载方式的url
     */
    public String getFileURL(String bucketName, String objectName, int expireSeconds) {
        try {
            String presignedObjectUrl = minioClient.getPresignedObjectUrl(
                    GetPresignedObjectUrlArgs.builder()
                            .method(Method.GET)
                            .bucket(bucketName)
                            .object(objectName)
                            .expiry(expireSeconds)
                            .build());
            return getUtf8ByURLDecoder(presignedObjectUrl);
        } catch (Exception e) {
            log.error("Get URL of object [{}] from bucket [{}] failed.", objectName, bucketName);
            return null;
        }
    }

    /**
     * 将URLDecoder编码转成UTF8
     *
     * @param str
     */
    protected String getUtf8ByURLDecoder(String str) {
        try {
            String url = str.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
            return URLDecoder.decode(url, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 上传文件
     *
     * @param bucketName 桶名称
     * @param objectName 存储对象名称
     * @param fileName   本地文件名称
     */
    public boolean uploadFile(String bucketName, String objectName, String fileName) {
        try {
            ObjectWriteResponse objectWriteResponse = minioClient.uploadObject(
                    UploadObjectArgs.builder()
                            .bucket(bucketName)
                            .object(objectName)
                            .filename(fileName)
                            .build());
            return true;
        } catch (Exception e) {
            log.error("Upload file [{}] to bucket [{}] failed.", fileName, bucketName);
            return false;
        }
    }

    @Data
    public static class ObjectItem {
        private String objectName;
        private Long size;
    }
}

