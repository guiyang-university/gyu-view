package com.gyu.common.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2023-12-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("map_pic")
public class MapPic implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 地图名称
     */
    @TableField("map_name")
    private String mapName;

    /**
     * 地图类型 1 图片 2 地图服务
     */
    @TableField("map_type")
    private String mapType;

    /**
     * 服务引擎
     */
    @TableField("engine")
    private String engine;

    /**
     * 服务类型
     */
    @TableField("service_type")
    private String serviceType;

    /**
     * 服务地址
     */
    @TableField("service_url")
    private String serviceUrl;

    /**
     * 地图显示范围
     */
    @TableField("rectangle")
    private String rectangle;

    /**
     * 地图图片
     */
    @TableField("img")
    private String img;

}
