package com.gyu.system.camera.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gyu.common.domain.CameraInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2023-12-25
 */
public interface CameraInfoService extends IService<CameraInfo> {

    //cameraName, cameraCode, cameraLocation,
    IPage findCameraByPage(String cameraName, String cameraCode, String cameraLocation, Integer pageNum, Integer pageSize);

    boolean createCamera(CameraInfo cameraInfo);

    boolean deleteCameraById(List<String> ids);

    boolean updateCameraById(CameraInfo cameraInfo);

    List<CameraInfo> findAll();
}
