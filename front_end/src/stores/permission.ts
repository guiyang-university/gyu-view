import {ref, computed, shallowRef} from 'vue'
import {defineStore} from 'pinia'
import {v4 as uuidv4} from 'uuid'

export const usePermissionStore = defineStore('permission', () => {
    const permissions = ref<string[]>([])

    const userMenus = shallowRef([
        {
            id: uuidv4(),
            menuName: '地图管理',
            icon: 'gis:map-book',
            menuType: 2,
            path: '/systemSetting/mapManager',
        },
        {
            id: uuidv4(),
            menuName: '硬件接入',
            icon: 'ion:hardware-chip-sharp',
            menuType: 1,
            path: '',
            children: [
                {
                    id: uuidv4(),
                    icon: 'icon-park-solid:camera-one',
                    menuName: '摄像头管理',
                    menuType: 2,
                    path: '/systemSetting/CameraManager',
                },
                {
                    id: uuidv4(),
                    icon: 'icon-park-solid:camera-one',
                    menuName: '重点区域监控',
                    menuType: 2,
                    path: '/systemSetting/important',
                },
                {
                    id: uuidv4(),
                    icon: 'ic:twotone-local-police',
                    menuName: '报警主机',
                    menuType: 2,
                    path: '/systemSetting/policeHost',
                },
                {
                    id: uuidv4(),
                    icon: 'academicons:closed-access-square',
                    menuName: '门禁管理',
                    menuType: 2,
                    path: '/systemSetting/accessSquare',
                },
                {
                    id: uuidv4(),
                    icon: 'tabler:route-alt-left',
                    menuName: '巡更点管理',
                    menuType: 2,
                    path: '/systemSetting/routeAltLeft',
                },
            ],
        },
    ])

    function setPermissions(permission: string[]) {
        permissions.value = permission
    }

    return {permissions, userMenus, setPermissions}
})
