//@ts-nocheck
import {shallowRef} from 'vue'
import {defineStore} from 'pinia'
import * as Cesium from 'cesium'
import GlobalBkLayerUrl from '@/assets/image/GlobalBkLayer.jpg'
// import {StatusBar} from './widget'
import {loadAreaEdge, load3DTileset} from './utils'
import {loadGeoServerTileMap, addLandmark, addRoad} from './business'
import mask from './effect/mask'
import {modifyMapLayerStyle} from './effect'
import PartMask from './effect/boundaryMaks'
import edgeData from './data/GYU_Edge.json'

export const useCesium = defineStore('cesiumViewer', () => {
    let viewer = shallowRef<NonNullable<Cesium.Viewer>>()
    // let statusBar: StatusBar
    /**
     * 初始化cesium.viewer
     * @param cesiumContainer 容器
     */
    function initCesium(cesiumContainer: HTMLElement) {
        return new Promise(async (resolve) => {
            const globalBkImageryProvider = new Cesium.SingleTileImageryProvider({
                url: GlobalBkLayerUrl,
                tileWidth: 10,
                tileHeight: 10,
            })
            const globalBkImageryLayer = new Cesium.ImageryLayer(globalBkImageryProvider, {})

            viewer.value = new Cesium.Viewer(cesiumContainer, {
                animation: false,
                baseLayerPicker: false,
                fullscreenButton: false,
                geocoder: false,
                homeButton: false,
                infoBox: false,
                sceneModePicker: false,
                navigationHelpButton: false,
                scene3DOnly: true,
                timeline: false,
                // globe: false,
                selectionIndicator: false, //是否显示选取指示器组件
                shouldAnimate: false, //自动播放动画控件
                terrainShadows: Cesium.ShadowMode.RECEIVE_ONLY, //地质接收阴影
                sceneMode: Cesium.SceneMode.SCENE3D,
                baseLayer: globalBkImageryLayer,
                terrainProvider: await Cesium.CesiumTerrainProvider.fromUrl(
                    // 'http://192.168.31.173:7911/terrain/gy',
                    'http://127.0.0.1:7911/terrain/gy',
                    // 'http://localhost:7911/terrain/gy',
                ),
                contextOptions: {
                    ebgl: {
                        alpha: true,
                        depth: true,
                        stencil: true,
                        antialias: true,
                        premultipliedAlpha: true,
                        //通过canvas.toDataURL()实现截图需要将该项设置为true
                        preserveDrawingBuffer: true,
                        failIfMajorPerformanceCaveat: true,
                    },
                },
            })
            // viewer.scene.globe.show = false

            // 开启深度测试，避免坐标拾取不正确
            viewer.value.scene.globe.depthTestAgainstTerrain = true

            viewer.value.cesiumWidget.creditContainer.setAttribute('style', 'display:none')

            // 加载贵阳学院瓦片
            const bounds = {
                east: 106.78092822432389,
                north: 26.565191685242382,
                south: 26.555978297276226,
                west: 106.76788330077996,
            }
            const gyuTileImageryProvider = new Cesium.UrlTemplateImageryProvider({
                // url: 'http:192.168.31.173:7911/imagery/gy/{z}/{x}/{y}.png',
                url: 'http://127.0.0.1:7911/imagery/gy/{z}/{x}/{y}.png',
                // url: 'http://192.168.31.173:9003/image/wmts/iQk4FRXJ/{z}/{x}/{y}',
                rectangle: Cesium.Rectangle.fromDegrees(
                    bounds.west,
                    bounds.south,
                    bounds.east,
                    bounds.north,
                ),
                minimumLevel: '0',
                maximumLevel: '19',
                tilingScheme: new Cesium.GeographicTilingScheme(),
            })

            // viewer.value.imageryLayers.addImageryProvider(gyuTileImageryProvider)

            loadGeoServerTileMap().forEach((tile) => {
                viewer.value?.imageryLayers.addImageryProvider(tile)
            })

            resolve()
        })
    }

    function initBusiness() {
        setTimeout(() => {
            // 地图局部遮罩
            // new PartMask(viewer.value).initBoundary(edgeData)

            // 限制相机移动
            // viewer.value.scene.screenSpaceCameraController.maximumZoomDistance = 5000 * 2
            viewer.value.scene.screenSpaceCameraController.minimumZoomDistance = 0

            // statusBar = new StatusBar(viewer.value, cesiumContainer)

            // 修改地图风格
            modifyMapLayerStyle(viewer.value)

            // 加载边界
            loadAreaEdge(viewer.value)
            // mask(viewer.value)

            // 加载瓦片
            // load3DTileset(viewer.value, 'http:192.168.31.173:7911/gyut3/tileset.json', -5)
            load3DTileset(viewer.value, 'http:127.0.0.1:7911/gyut3/tileset.json', -5)

            // 加载地标
            addLandmark(viewer.value)

            // 加载路网
            addRoad(viewer.value)
        }, 500)
    }

    /**
     * 销毁cesium
     */
    function destroyCesium() {
        // if (statusBar) {
        //     statusBar.destroy()

        //     statusBar = null
        // }
        if (viewer.value) {
            viewer.value.destroy()
        }
    }

    return {viewer, initCesium, destroyCesium, initBusiness}
})
