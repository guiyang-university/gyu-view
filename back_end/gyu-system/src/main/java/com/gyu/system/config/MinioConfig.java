package com.gyu.system.config;

import io.minio.MinioClient;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Data //lombok插件生成get set方法
@Component //注入一个组件
@ConfigurationProperties(prefix = "minio") //以minio开头的字符串映射
@Slf4j
public class MinioConfig {
    private String endpoint;
    private String accessKey;
    private String secretKey;

    /**
     * 在spring容器中注入一个bean实例，MinioClient链接minio的链接对象
     * @return
     */
    @Bean
    public MinioClient minioClient(){
        log.info("初始化MinioClient客户端：endpoint:" + endpoint + ",accessKey:" + accessKey + ",secretKey:" + secretKey);
        MinioClient minioClient = MinioClient.builder()
                .endpoint(endpoint)
                .credentials(accessKey,secretKey)
                .build();
        return minioClient;
    }
}
