package com.gyu.common.utils;


import com.alibaba.fastjson.JSONObject;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.Date;

public class ReflectUtils {

    /**
     * 封装请求头中的数据
     * @param obj
     * @return
     * @throws Exception
     */
    public static void copySaveProperty(Object obj) {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String authorization = request.getHeader("Authorization");
            JSONObject jsonObject = null /*JwtUtils.getUserInfo(authorization)*/;
            Long id = jsonObject.getLong("id");
            String name = jsonObject.getString("name");
            short status = 1;
            Class<?> objClass = obj.getClass();
            BeanInfo beanInfo = Introspector.getBeanInfo(objClass);
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();

            for (int i = 0; i < propertyDescriptors.length; i++) {
                PropertyDescriptor propertyDescriptor = propertyDescriptors[i];
                String fieldName = propertyDescriptor.getName();
                Method method = propertyDescriptor.getWriteMethod();
                if(fieldName.equalsIgnoreCase("creatorId")){
                    method.invoke(obj,id);
                }else if(fieldName.equalsIgnoreCase("creator")){
                    method.invoke(obj,name);
                }else if(fieldName.equalsIgnoreCase("updaterId")){
                    method.invoke(obj,id);
                }else if(fieldName.equalsIgnoreCase("updater")){
                    method.invoke(obj,name);
                }else if(fieldName.equalsIgnoreCase("createTime")){
                    method.invoke(obj,new Date());
                }else if(fieldName.equalsIgnoreCase("updateTime")){
                    method.invoke(obj,new Date());
                }else if(fieldName.equalsIgnoreCase("status")){
                    method.invoke(obj,status);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
           throw  new RuntimeException(e);
        }
    }

    /**
     * 封装请求头中的数据
     * @param obj
     * @return
     * @throws Exception
     */
    public static void copyUpdateProperty(Object obj){
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String authorization = request.getHeader("Authorization");
            JSONObject jsonObject = null;/*JwtUtils.getUserInfo(authorization);*/
            Long id = jsonObject.getLong("id");
            String name = jsonObject.getString("name");
            Class<?> objClass = obj.getClass();
            BeanInfo beanInfo = Introspector.getBeanInfo(objClass);
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();

            for (int i = 0; i < propertyDescriptors.length; i++) {
                PropertyDescriptor propertyDescriptor = propertyDescriptors[i];
                String fieldName = propertyDescriptor.getName();
                Method method = propertyDescriptor.getWriteMethod();
                if(fieldName.equalsIgnoreCase("updaterId")){
                    method.invoke(obj,id);
                }else if(fieldName.equalsIgnoreCase("updater")){
                    method.invoke(obj,name);
                }else if(fieldName.equalsIgnoreCase("updateTime")){
                    method.invoke(obj,new Date());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw  new RuntimeException(e);
        }
    }
}
