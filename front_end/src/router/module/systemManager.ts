import {type RouteRecordRaw} from 'vue-router'
import Layout from '../../layout/index.vue'
const SystemRoute: RouteRecordRaw[] = [
    {
        path: '/',
        name: 'Layout',
        component: Layout,
        redirect: '/map',
        children: [
            {
                path: '/systemSetting',
                name: 'systemSetting',
                component: () => import('@/views/private/SystemSetting/SystemSetting.vue'),
                meta: {
                    title: '系统设置',
                },
                redirect: '/systemSetting/mapManager',
                children: [
                    {
                        path: '/systemSetting/mapManager',
                        name: 'mapManager',
                        component: () =>
                            import('@/views/private/SystemSetting/MapManager/index.vue'),
                        meta: {
                            title: '地图管理',
                        },
                    },
                    {
                        path: '/systemSetting/mapManager/addMap',
                        name: 'addMap',
                        component: () =>
                            import('@/views/private/SystemSetting/MapManager/addMap.vue'),
                        meta: {
                            title: '添加地图',
                            activeMenu: '/systemSetting/mapManager',
                        },
                    },
                    {
                        path: '/systemSetting/CameraManager',
                        name: 'hardwareManager',
                        component: () =>
                            import(
                                '@/views/private/SystemSetting/HardwareManager/CameraManager.vue'
                            ),
                        meta: {
                            title: '摄像头管理',
                        },
                    },
                    {
                        path: '/systemSetting/CameraManager/AddCamera',
                        name: 'CameraManager',
                        component: () =>
                            import('@/views/private/SystemSetting/HardwareManager/AddCamera.vue'),
                        meta: {
                            title: '添加摄像头',
                            activeMenu: '/systemSetting/CameraManager',
                        },
                    },
                    {
                        path: '/systemSetting/important',
                        name: 'CameraImportant',
                        component: () =>
                            import('@/views/private/SystemSetting/HardwareManager/important.vue'),
                        meta: {
                            title: '添加摄像头',
                            activeMenu: '/systemSetting/important',
                        },
                    },
                ],
            },
            {
                path: '/map',
                name: 'map',
                component: () => import('@/views/private/ElectronMap/ElectronMap.vue'),
                meta: {
                    title: '电子地图',
                },
            },
        ],
    },
]

export default SystemRoute
