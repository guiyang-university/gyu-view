/*
 * @Author: Wang Wei 2419715149
 * @Date: 2023-09-03 11:06:49
 * @LastEditors: ww 92363803+lisrmww@users.noreply.github.com
 * @LastEditTime: 2023-09-03 11:28:41
 * @Description: 全局指令注册
 */
import type {App} from 'vue'

export default {
    install(app: App<Element>) {
        const directives = import.meta.glob('./modules/*.ts', {eager: true})
        for (const [key, value] of Object.entries(directives)) {
            // 拼接组件注册的 name
            const arr = key.split('/')
            const directiveName = arr[arr.length - 1].replace('.ts', '')
            
            // 完成注册
            app.directive(directiveName, (value as any).default)
        }
    },
}
