import request from '..'

const BASE_URL = '/camera/cameraInfo/'

/**
 * 获取所有摄像头
 */
export function getAllCamreas() {
    return request.get({
        url: `${BASE_URL}cameras`,
        isShowLoading: false,
    })
}

/**
 * 获取摄像头列表
 */
export function getCameraList(params: {keyword?: string; pageNum?: number; pageSize?: number}) {
    return request.get({
        url: `${BASE_URL}list`,
        params,
    })
}

/**
 * 新增摄像头
 */
export function createCamera(data: any) {
    return request.post({
        url: `${BASE_URL}create`,
        data,
    })
}

/**
 * 根据摄像头ids批量删除
 */
export function batchDeleteCameraByIdS(ids: string[]) {
    return request.post({
        url: `${BASE_URL}delete`,
        data: {
            ids,
        },
    })
}

export function updateCamera(data: any) {
    return request.post({
        url: `${BASE_URL}update`,
        data,
    })
}

/**
 * 同步海康摄像头
 */
export function syncHikCameraTree() {
    return request.post({
        baseURL: '/gyu',
        url: '/guiyangcollege/mon/camera/pageCamera',
        data: {},
    })
}

export function getCameraTree() {
    return request.get({
        baseURL: '/gyu',
        url: '/guiyangcollege/mon/camera/find/tree',
    })
}

export function editCameraPosition(data: any) {
    return request.post({
        baseURL: '/gyu',
        url: '/guiyangcollege/mon/camera/editCameraById',
        data,
    })
}

// 查询重点区域监控点
export function getImportantCamera(params = {}) {
    return request.get({
        baseURL: '/gyu',
        url: '/guiyangcollege/mon/camera/find/keyMonitoringCameraList',
        params: params,
    })
}

// 获取监控点预览url
export function getPreviewUrl(params = {}) {
    return request.get({
        baseURL: '/gyu',
        url: '/guiyangcollege/mon/camera/preview',
        params,
    })
}

// 获取监控点在线状态统计
export function getCameraStatus() {
    return request.get({
        baseURL: '/gyu',
        url: '/guiyangcollege/mon/statistic/online',
    })
}