package com.gyu.system.gyucamera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GyuCameraApplication {

    public static void main(String[] args) {
        SpringApplication.run(GyuCameraApplication.class, args);
    }

}
