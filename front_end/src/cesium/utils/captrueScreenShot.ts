/**
 * 出图
 */

import * as Cesium from 'cesium'

export function captureScreenShot(viewer: Cesium.Viewer) {
    viewer.scene.render()
    let canvas = viewer.scene.canvas
    let image = canvas.toDataURL('image/png').replace('image/png', 'image/octet-stream')
    //base64转blob
    let blob = dataURLtoBlob(image)
    // let objUrl = URL.createObjectURL(blob)
    return blob
}

function dataURLtoBlob(dataUrl: any) {
    let arr = dataUrl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        base64Str = atob(arr[1]),
        n = base64Str.length,
        u8arr = new Uint8Array(n)
    while (n--) {
        u8arr[n] = base64Str.charCodeAt(n)
    }
    return new Blob([u8arr], {type: mime})
}
