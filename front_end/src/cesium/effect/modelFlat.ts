import * as Cesium from "cesium";

/**
 * ospf模型压平效果
 * 实现步骤：
 * 1、采集目标区域多边形轮廓
 * 2、采集目标高度
 * 3、
 *
 */
export default class CesiumModelFlat {
  viewer: Cesium.Viewer;
  floatingPoint: Cesium.Entity | undefined;
  polygonPoints: any[]; // 存储绘制多边形的点
  polygonPrimitive: Cesium.Entity | undefined; // entity最终会转换为primitive的形式
  constructor(viewer: Cesium.Viewer) {
    this.viewer = viewer;
    this.floatingPoint = undefined;
    this.polygonPoints = [];
    this.polygonPrimitive = undefined;
  }

  /**
   * 1、采集目标区域多边形轮廓
   */
  drawPolygon() {
    const handler = this.viewer.screenSpaceEventHandler;

    // 清除所有事件
    handler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_CLICK);
    handler.removeInputAction(Cesium.ScreenSpaceEventType.RIGHT_CLICK);
    handler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE);

    if (Cesium.defined(this.floatingPoint)) {
      this.viewer.entities.remove(this.floatingPoint as Cesium.Entity);
      this.floatingPoint = undefined;
      this.polygonPoints = [];
    }

    // 鼠标左键点击取点
    handler.setInputAction((evt: any) => {
      // 获取场景中的世界坐标
      const earthPosition = this.viewer.scene.pickPosition(evt.position);
      if (Cesium.defined(earthPosition)) {
        if (this.polygonPoints.length === 0) {
          this.polygonPoints.push(earthPosition);
        }
        this.polygonPoints.push(earthPosition);
        // 没有定义绘制一个点，存在则不绘制
        if (!Cesium.defined(this.floatingPoint)) {
          this.floatingPoint = drawPoint();
        }
        // 开始绘制
        if (!Cesium.defined(this.polygonPrimitive)) {
          this.polygonPrimitive = this.viewer.entities.add({
            polyline: {
              clampToGround: true,
              width: 3,
              material: Cesium.Color.WHITE.withAlpha(0.7),
              positions: new Cesium.CallbackProperty(() => {
                return this.polygonPoints;
              }, false)
            }
          });
        }
      }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);

    // 鼠标移动的结束的位置更新数组中的点
    handler.setInputAction((evt: any) => {
      if (Cesium.defined(this.floatingPoint)) {
        const newPosition = this.viewer.scene.pickPosition(evt.endPosition);
        if (Cesium.defined(newPosition)) {
          this.polygonPoints.pop();
          this.polygonPoints.push(newPosition);
        }
      }
    }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

    // 双击结束绘制
    handler.setInputAction(() => {
      this.polygonPoints.pop();
      this.polygonPoints.pop();
      if (this.polygonPoints.length) {
        // 使用图元的方式绘制为静态图
        addPloygonGeometry();
      }

      if (Cesium.defined(this.floatingPoint) && Cesium.defined(this.polygonPrimitive)) {
        this.viewer.entities.remove(this.floatingPoint as Cesium.Entity); //去除动态点图形（当前鼠标点）
        this.viewer.entities.remove(this.polygonPrimitive as Cesium.Entity);

        this.floatingPoint = undefined;
        // this.polygonPoints = [];
        this.polygonPrimitive = undefined;
      }
      handler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_CLICK);
      handler.removeInputAction(Cesium.ScreenSpaceEventType.RIGHT_CLICK);
      handler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE);
      handler.destroy();
    }, Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK);

    /**
     * 绘制一个跟随鼠标的点
     * @returns point
     */
    const drawPoint = () => {
      const point = this.viewer.entities.add({
        position: new Cesium.CallbackProperty(() => {
          return this.polygonPoints[this.polygonPoints.length - 1];
        }, false) as any,
        point: {
          pixelSize: 10,
          color: Cesium.Color.RED.withAlpha(0.8),
          outlineColor: Cesium.Color.WHITE,
          outlineWidth: 1,
          heightReference: Cesium.HeightReference.CLAMP_TO_GROUND // 贴地
        }
      });

      return point;
    };

    /**
     * 使用图元的方式绘制多边形
     */
    const addPloygonGeometry = () => {
      const polygonGeometry = new Cesium.PolygonGeometry({
        polygonHierarchy: new Cesium.PolygonHierarchy(this.polygonPoints)
      });

      const polygonInstance = new Cesium.GeometryInstance({
        geometry: polygonGeometry,
        attributes: {
          color: Cesium.ColorGeometryInstanceAttribute.fromColor(Cesium.Color.WHITE.withAlpha(0.7))
        }
      });

      const groundPolygonPrimitive = new Cesium.GroundPrimitive({
        geometryInstances: polygonInstance,
        asynchronous: false, // 是否采用多线程
        appearance: new Cesium.PerInstanceColorAppearance({
          flat: true,
          translucent: true
        })
      });

      this.viewer.scene.primitives.add(groundPolygonPrimitive);
    };
  }

  /**
   * 清除绘制的多边形轮廓
   */
  clear() {}

  /**
   * 2、获取目标高度
   * @returns height
   */
  getTargetHeight() {
    return 100;
  }

  /**
   * 3、调用Cesium.Framebuffer对象渲染一张矩形Texture，且叠加区域内的灰度为1，多边形外为0
   */
  generateReactTexure() {
    // 根据多边形的顶点计算矩形
    const computedReact = "";
  }
}
