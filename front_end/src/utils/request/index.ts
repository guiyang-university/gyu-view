import axios from 'axios'
import type {AxiosInstance} from 'axios'
import type {RequestConfig} from './types'
// import {ElLoading} from 'element-plus'

export default class Request {
    private instance: AxiosInstance
    private isShowLoading: boolean
    private loading: any

    constructor(config: RequestConfig) {
        this.instance = axios.create(config)
        this.isShowLoading = config.isShowLoading ?? true

        // 默认注册的拦截器
        this.instance.interceptors.request.use(
            (config) => {
                // loading动画
                if (this.isShowLoading) {
                    this.loading = ElLoading.service({
                        lock: true,
                        text: '数据加载中...',
                        background: 'rgba(0, 0, 0, 0.7)',
                    })
                }
                return config
            },
            (error) => {
                return Promise.reject(error)
            },
        )
        this.instance.interceptors.response.use(
            (result) => {
                // 关闭loading动画
                if (this.isShowLoading) {
                    this.loading.close()
                }
                return result.data
            },
            (error) => {
                // 关闭loading动画
                if (this.isShowLoading) {
                    this.loading.close()
                }

                const statusCode = error.response ? error.response.status : 404

                switch (statusCode) {
                    case 401:
                        ElMessage.error('未授权，请登录')
                        break
                    case 403:
                        ElMessage.error('暂无查看权限')
                        break
                    case 404:
                        ElMessage.error(`请求地址出错: ${error.response.config.url}`)
                        break
                    case 500:
                        ElMessage.error('服务器错误')
                        break
                    case 502:
                        ElMessage.error('网关错误')
                        break
                    case 504:
                        ElMessage.error('网关超时')
                        break
                    default:
                        ElMessage.error('网络连接错误')
                        break
                }

                return Promise.reject(error)
            },
        )

        // 特定axios请求实例的拦截器
        // 针对特定实例添加统一的请求头，列如token
        this.instance.interceptors.request.use(
            config.interceptors?.requestInterceptors,
            config.interceptors?.requestInterceptorsCatch,
        )
        this.instance.interceptors.response.use(
            config.interceptors?.responseInterceptors,
            config.interceptors?.responseInterceptorsCatch,
        )
    }
    request<T = any>(config: RequestConfig<T>): Promise<T> {
        return new Promise((resolve, reject) => {
            // 单个请求的拦截器
            if (config.interceptors?.requestInterceptors) {
                config = config.interceptors.requestInterceptors(config)
            }
            if (config.isShowLoading === false) {
                this.isShowLoading = false
            }
            this.instance
                .request<any, T>(config)
                .then((res) => {
                    if (config.interceptors?.responseInterceptors) {
                        res = config.interceptors.responseInterceptors(res)
                    }
                    this.isShowLoading = true
                    resolve(res)
                })
                .catch((err) => {
                    this.isShowLoading = true
                    reject(err)
                    return err
                })
        })
    }

    get<T = any>(config: RequestConfig<T>): Promise<T> {
        return this.request<T>({...config, method: 'GET'})
    }

    post<T = any>(config: RequestConfig<T>): Promise<T> {
        return this.request<T>({...config, method: 'POST'})
    }

    delete<T = any>(config: RequestConfig<T>): Promise<T> {
        return this.request<T>({...config, method: 'DELETE'})
    }

    patch<T = any>(config: RequestConfig<T>): Promise<T> {
        return this.request<T>({...config, method: 'PATCH'})
    }

    put<T = any>(config: RequestConfig<T>): Promise<T> {
        return this.request<T>({...config, method: 'PUT'})
    }
}
