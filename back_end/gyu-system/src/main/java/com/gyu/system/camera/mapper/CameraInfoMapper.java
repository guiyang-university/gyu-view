package com.gyu.system.camera.mapper;

import com.gyu.common.domain.CameraInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-12-25
 */
public interface CameraInfoMapper extends BaseMapper<CameraInfo> {

}
