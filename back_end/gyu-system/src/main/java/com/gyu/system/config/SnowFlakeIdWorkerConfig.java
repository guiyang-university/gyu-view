package com.gyu.system.config;

import com.gyu.common.utils.SnowFlakeIdWorker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 雪花算法生成分布式主键id
 */
@Configuration
public class SnowFlakeIdWorkerConfig {
    @Bean
    public SnowFlakeIdWorker snowFlakeIdWorker(){
        return new SnowFlakeIdWorker(1,1);
    }
}
