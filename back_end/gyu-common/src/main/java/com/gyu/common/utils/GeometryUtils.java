package com.gyu.common.utils;

import org.geotools.geojson.geom.GeometryJSON;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.WKTReader;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

public class GeometryUtils {

    public static Geometry wktToGeometry(String wkt) {
        Geometry geometry = null;
        try {
            WKTReader reader = new WKTReader();
            geometry = reader.read(wkt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return geometry;
    }

    public static String wktToGeoJson(String wkt) {
        String json = null;
        try {
            WKTReader reader = new WKTReader();
            Geometry geometry = reader.read(wkt);
            StringWriter writer = new StringWriter();
            GeometryJSON g = new GeometryJSON(9);
            g.write(geometry, writer);
            json = writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public static String geoJsonToWkt(String geoJson) {
        String wkt = null;
        GeometryJSON g = new GeometryJSON(20);
        StringReader reader = new StringReader(geoJson);
        try {
            Geometry geometry = g.read(reader);
            wkt = geometry.toText();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return wkt;
    }
}
