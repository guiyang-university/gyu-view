import request from '..'

/**
 * 上传静态图片作为地图
 */
export function uploadMapPic(formData: any) {
    return request.post({
        timeout: 30000,
        url: '/map/mapPic/upload',
        data: formData,
        headers: {'Content-Type': 'multipart/form-data'},
    })
}

/**
 * 保存地图
 */
export function saveMap(data: any) {
    return request.post({
        url: '/map/mapPic/save',
        data: data,
    })
}

/**
 * 获取地图列表
 */
export function getMapList(params: {
    mapName?: string
    mapType?: string
    pageNum?: number
    pageSize?: number
}) {
    return request.get({
        url: '/map/mapPic/list',
        params,
    })
}

/**
 * 获取所有图层
 */
export function getAllLayers() {
    return request.get({
        url: `/map/mapPic/layers`,
        isShowLoading: false,
    })
}

//添加地图
export const addMap = (data:any) =>{
    return request.post({
        baseURL: "/gyu",
        url:'/guiyangcollege/mon/map/add',
        data
    })
}

// 获取地图分页
export const getMapPage = (params:any)=>{
    return request.get({
        url:'/guiyangcollege/mon/map/page',
        baseURL: "/gyu",
        params:params
    })
}


// 上传文件
export const uploadFile = (formData:any
) =>{
    return request.post({
        baseURL: "/gyu",
        timeout: 30000,
        url: '/guiyangcollege/mon/file/add',
        data: formData,
        headers: {'Content-Type': 'multipart/form-data'},
    })
}